import React from "react";
import { Header } from "react-navigation";
import { View, Platform } from "react-native";

const CustomHeader = props => {
  return (
    <View
      style={{
        height: 80,
        marginTop: Platform.OS == "ios" ? 20 : 0,
        backgroundColor: 'grey'
      }}
    >

        <Header {...props} />

    </View>
  );
};

export default CustomHeader;
