import React from 'react';
import { StyleSheet, Text, View,
          TextInput, Container,
          TouchableOpacity, AsyncStorage,
          Navigator, AppRegistry
        } from 'react-native';
import { createStackNavigator } from 'react-navigation';

 class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      pincode: "1234"
    };

    this._login = this._login.bind(this);
  }

  _login(){
    console.log('test');
  }
  componentDidMount() {
    const { countries, fetchAllCountries } = this.props;
    if (!countries || (countries && countries.length === 0)) {
      fetchAllCountries();
    }
    this.setState({ pincode: 1234 });
  }
  render() {
      return (
                  <View style={styles.container}>

                            <View style={styles.content}>

                                <Text style={styles.logo}>- Native -</Text>
                                        <View style={styles.inputContainer}>

                                            <TextInput
                                             style={styles.textInput} placeholder='Username'
                                             underlineColorAndroid='transparent'>
                                             </TextInput>

                                             <TextInput
                                              style={styles.textInput} placeholder='Password'
                                               underlineColorAndroid='transparent'>
                                               </TextInput>

                        <TouchableOpacity
                          style={styles.btn}
                          onPress={this._login}>
                          <Text>Login </Text>
                          </TouchableOpacity>

                                        </View>
                          </View>
                      <Text>Do not have account? Sign Up here!</Text>

                  </View>

      );}}
export default Login;
  const styles = StyleSheet.create({

    container: {
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
            justifyContent: 'flex-end',
            alignItems: 'center'

    },
    textinput: {
          alignSelf: 'stretch',
          height: 40,
          marginBottom: 30,
          color: '#fff',
          borderBottomColor: '#f8f8f8',
          borderBottomWidth: 1,

        },
    button: {
     alignSelf: 'stretch',
     alignItems: 'center',
     padding: 20,
     backgroundColor: '#59cbbd',
     marginTop: 30,
   },
   btntext:{
     color: '#fff',
     fontWeight: 'bold'
   }
  });
