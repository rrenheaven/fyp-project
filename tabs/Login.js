import React from 'react';
import { StyleSheet, Text, View, TextInput,
          TouchableOpacity, AsyncStorage,
              Navigator, AppRegistry } from 'react-native';
import { createStackNavigator } from 'react-navigation';

export default class Login extends React.Component {

  render() {
    return (


      <View style={styles.container}>

                <View style={styles.content}>

                    <Text style={styles.logo}>- Native -</Text>
                            <View style={styles.inputContainer}>

                                <TextInput
                                 style={styles.textInput} placeholder='Username'
                                 underlineColorAndroid='transparent'>
                                 </TextInput>

                                 <TextInput
                                  style={styles.textInput} placeholder='Password'
                                   underlineColorAndroid='transparent'>
                                   </TextInput>

            <TouchableOpacity
              style={styles.btn}
              onPress={this._login}>
              <Text>Login </Text>
              </TouchableOpacity>

      </View>

        <Text style={styles.logo}>- Do you have account? Sign Up. -</Text>
    </View>

      </View>

);
}

}

const styles = StyleSheet.create({
      container: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#36485f',
        flex: 1,

      },
      textinput: {
          alignSelf: 'stretch',
          height: 40,
          marginBottom: 30,
          color: '#fff',
          borderBottomColor: '#f8f8f8',
          borderBottomWidth: 1,

        },
    button: {
     alignSelf: 'stretch',
     alignItems: 'center',
     padding: 20,
     backgroundColor: '#59cbbd',
     marginTop: 30,
   },

  btntext:{
     color: '#fff',
     fontWeight: 'bold'
   }

});
