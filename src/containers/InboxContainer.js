import React, { Component } from 'react';
import {StyleSheet,Text,View,AppRegistry,TouchableHighlight,
  ListView,Animated,ScrollView,TouchableOpacity,RefreshControl ,ActivityIndicator, Image,FlatList, Alert, YellowBox}
from 'react-native';
import { Card, CardItem, Button, Icon } from 'react-native-elements'
import Planlist from '../components/plan/plan_list'
import Packlist from '../components/packages/packages_list'
import Swiper from 'react-native-swiper'
import Accordio from '../components/packages/AccordionPac';
import AccordioPlanner from '../components/plan/AccordioPlanner';
import Timeline from 'react-native-timeline-listview'

 class InboxContainer extends Component {

   constructor(props) {

      super(props);

      this.state = {

        refreshing: false,
          ActivityIndicator_Loading: false,
      }
      this.webCall();


      YellowBox.ignoreWarnings([
       'Warning: componentWillMount is deprecated',
       'Warning: componentWillReceiveProps is deprecated',
     ]);

    }






    webCall=()=>{

      return fetch('http://192.168.1.253/User_Project/inbox_list.php')
             .then((response) => response.json())
             .then((responseJson) => {
               this.setState({
                 isLoading: false,
                 dataplan: responseJson
               }, function() {
                 // In this block you can do something with new state.
               });
             })
             .catch((error) => {
               console.error(error);
             });


     }

     webCallpack=()=>{

       return fetch('http://192.168.1.253/User_Project/inbox_list2.php')
              .then((response) => response.json())
              .then((responseJson) => {

                this.setState({
                  isLoading: false,
                  datapack: responseJson,
                }, function() {
                  // In this block you can do something with new state.
                });
              })
              .catch((error) => {
                console.error(error);
              });

      }

    componentDidMount(){
      console.log('Get', this.props.navigation.state.params);
      this.webCall();
      this.webCallpack();
      this.setState({
        dataP:
((typeof this.props.navigation.state.params === "undefined") ? '' : this.props.navigation.state.params.dataP),
      })
    }

 // _addPlanner(){
 //         console.log('Add',this.props.navigation.state.params);
 //         this.props.navigation.navigate('Inbox', {
 //                  datafinal: this.props.navigation.state.params,//store page data
 //                })
 //              }
 _onRefresh = () => {
    this.webCall();
    this.webCallpack();

   }

   render() {
     // const { navigation } = this.props;
     // const startdate = navigation.getParam('startdate', '');
     // const dataHo = navigation.getParam('dataHo', '');
     // const dataRe = navigation.getParam('dataRe', '');
     // const dataAtt = navigation.getParam('dataAtt', '');
     //
     // // console.log('INBOX', this.state.props);

     return(
       <View>

	   <View>
     <Text style={{fontSize: 21,
     fontWeight: '600',
     marginBottom: 30,
     marginTop: 5,
     paddingLeft: 20,
     paddingRight: 20,
     margin: 5,
     padding:32,
   backgroundColor:'#ffffff'}}>
     Planner</Text>



      <ScrollView
      refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }>
      {(this.state.dataSource == '')
      ?
      <Text style={{fontSize: 21,
      fontWeight: '600',
      marginBottom: 30,
      marginTop: 5,
      paddingLeft: 20,
      paddingRight: 20,
      margin: 5,
      padding:32,
    backgroundColor:'#ffffff'}}>
      Plan OR Packages</Text>
    :
    <View>
    <Text style={{fontSize: 21,
    fontWeight: '600',
    marginBottom: 30,
    marginTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    margin: 5,
    padding:32,
    backgroundColor:'#ffffff'}}>
    Your Trips</Text>
    <FlatList

             data={ this.state.dataplan }
             enableEmptySections = {true}
             renderItem={({item}) =>

             <AccordioPlanner title="Is this Header text">
              <View style={{backgroundColor:'#f2f2f2',margin:10,padding:10}}>
              <Text>Startdate: {item.start_date_time}</Text>
              <Text>Hotel Name: {item.Hotel_name}</Text>
              <Text>Restaurant Name: {item.Rest_name}</Text>
              <Text>Attraction Name: {item.Att_name}</Text>

              </View>

              </AccordioPlanner>

               }

             keyExtractor={(item, index) => index.toString()}


             />

             <FlatList
                      data={ this.state.datapack }
                      enableEmptySections = {true}
                      renderItem={({item}) =>
                      <Accordio title="Is this Header text">
                       <View style={{backgroundColor:'#f2f2f2',margin:10,padding:10}}>
                       <Text>Area: {item.pack_area}</Text>
                       <Text>Hotel Name: {item.pack_hotel}</Text>
                       <Text>Restaurant Name: {item.pack_rest}</Text>
                       <Text>Attraction Name: {item.pack_site}</Text>

                       </View>

                       </Accordio>

                        }

                      keyExtractor={(item, index) => index.toString()}


                      />






     </View>

}

        </ScrollView>
      </View>
      </View>

     );
   }

 }

 const styles = StyleSheet.create({
   heading1: {
     fontSize: 15,
     fontWeight: '550',
 padding:20
   },
    heading2: {
       fontSize: 12,
       fontWeight: '550',
       color: 'black'
     },
     TouchableOpacityStyle:
  {
     padding:10,
     backgroundColor:'#009688',
     margin: 20,
     width: '20%',
     borderRadius:40,
     marginLeft: 180,



   },
   list: {
  flex: 1,
  marginTop:20,
},
     });
    export default InboxContainer;
AppRegistry.registerComponent('InboxContainer', () => InboxContainer);
