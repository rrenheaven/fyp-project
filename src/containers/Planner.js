import React, { Component } from 'react';
import {StyleSheet,Text,View,AppRegistry,TouchableHighlight,
  ListView,Animated,ScrollView,TouchableOpacity,ActivityIndicator, Image,FlatList, Alert, YellowBox}
from 'react-native';
import { Card, CardItem, Button, Icon } from 'react-native-elements'
import Planlist from '../components/plan/plan_list'
import Packlist from '../components/packages/packages_list'
import Swiper from 'react-native-swiper'
import Accordio from '../components/packages/AccordionPac';
import AccordioPlanner from '../components/plan/AccordioPlanner';

 class Planner extends Component {

   constructor(props) {

      super(props);

      this.state = {


          ActivityIndicator_Loading: false,
      }

      YellowBox.ignoreWarnings([
       'Warning: componentWillMount is deprecated',
       'Warning: componentWillReceiveProps is deprecated',
     ]);

    }



    componentDidMount(){
      console.log('Get', this.props.navigation.state.params);
      this.setState({
        dataP:
((typeof this.props.navigation.state.params === "undefined") ? '' : this.props.navigation.state.params.dataP),
      })


    }

 // _addPlanner(){
 //         console.log('Add',this.props.navigation.state.params);
 //         this.props.navigation.navigate('Inbox', {
 //                  datafinal: this.props.navigation.state.params,//store page data
 //                })
 //              }

Insert_Data_Into_MySQL = () =>
                  {
                      this.setState({ ActivityIndicator_Loading : true }, () =>
                      {
                          fetch('http://192.168.1.253/User_Project/planner_list.php',
                          {
                              method: 'POST',
                              headers:
                              {
                                  'Accept': 'application/json',
                                  'Content-Type': 'application/json',
                              },
                              body: JSON.stringify(
                              {

                                Hotel_name : this.props.navigation.state.params.dataHo,

                                Rest_name : this.props.navigation.state.params.dataRe,

                                Att_name : this.props.navigation.state.params.dataAtt,

                                Start_Date_Time: this.props.navigation.state.params.startdate,


                              })

                          }).then((response) => response.json()).then((responseJsonFromServer) =>
                          {
                              alert(responseJsonFromServer);

                              this.setState({ ActivityIndicator_Loading : false });

                          }).catch((error) =>
                          {
                              console.error(error);

                              this.setState({ ActivityIndicator_Loading : false});
                          });
                      });
                  }





   render() {
     const { navigation } = this.props;
     const startdate = navigation.getParam('startdate', '');
     const dataHo = navigation.getParam('dataHo', '');
     const dataRe = navigation.getParam('dataRe', '');
     const dataAtt = navigation.getParam('dataAtt', '');

     // console.log('INBOX', this.state.props);
     return(
       <View>

	   <View>
     <Text style={{fontSize: 21,
     fontWeight: '600',
     marginBottom: 30,
     marginTop: 5,
     paddingLeft: 20,
     paddingRight: 20,
     margin: 5,
     padding:32,
   backgroundColor:'#ffffff'}}>
     Planner</Text>



      <ScrollView>
      {(this.state.dataP == '')
      ?
      <Text style={{fontSize: 21,
      fontWeight: '600',
      marginBottom: 30,
      marginTop: 5,
      paddingLeft: 20,
      paddingRight: 20,
      margin: 5,
      padding:32,
    backgroundColor:'#ffffff'}}>
      Plan OR Packages</Text>
    :
    <View>

      <AccordioPlanner title="Is this Header text">

       <View style={{backgroundColor:'#f2f2f2',margin:10,padding:10}}>
       <Text>Startdate: {JSON.stringify(startdate)}</Text>
       <Text>Hotel Name: {JSON.stringify(dataHo)}</Text>
       <Text>Restaurant Name: {JSON.stringify(dataRe)}</Text>
       <Text>Attraction Name: {JSON.stringify(dataAtt)}</Text>
       </View>

       </AccordioPlanner>

       <TouchableOpacity
                 activeOpacity = { 0.5 }
                 style = { styles.TouchableOpacityStyle }
                 onPress = { this.Insert_Data_Into_MySQL }>

                   <Text style = { styles.TextStyle }>Confrim</Text>

               </TouchableOpacity>

     </View>

}

        </ScrollView>
      </View>
      </View>

     );
   }

 }

 const styles = StyleSheet.create({
   heading1: {
     fontSize: 15,
     fontWeight: '550',
 padding:20
   },
    heading2: {
       fontSize: 12,
       fontWeight: '550',
       color: 'black'
     },
     TouchableOpacityStyle:
  {
     padding:10,
     backgroundColor:'#009688',
     margin: 20,
     width: '20%',
     borderRadius:40,
     marginLeft: 180,



   },
     });
    export default Planner;
AppRegistry.registerComponent('Planner', () => Planner);
