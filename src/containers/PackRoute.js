import { createStackNavigator } from 'react-navigation';
import React, { Component } from 'react';

import InboxContainer from '../containers/InboxContainer';
import ProfileContainer from '../containers/ProfileContainer';
import LoggedOut from '../screens/LoggedOut';
import LogIn from '../screens/LogIn';


const PackRoute = createStackNavigator(
  {
    ProfileContainer: {screen: ProfileContainer},
    LoggedOut: {screen: LoggedOut},
    LogIn: {screen: LogIn},

  },
  {
    initialRouteName: 'ProfileContainer',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
      // tabBarBottomVisible: false
    },// this is needed to make sure header is hidden
  }
);


class PackInterface extends React.Component {
render(){
  return <PackRoute/>
}

};
export default PackRoute;
