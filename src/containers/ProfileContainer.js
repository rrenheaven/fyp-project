import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,FlatList,
  ImageBackground,Image,ScrollView,ActivityIndicator,Alert,
  TouchableOpacity,TextInput}
  from "react-native";
import { Card, CardItem, Thumbnail,
   Body, Left, Right, Button } from 'native-base';
   import {
     TabBarBottom,
     createBottomTabNavigator,
     createStackNavigator,
     createDrawerNavigator
   } from "react-navigation";
   import LoggedOut from '../screens/LoggedOut';


class ProfileContainer extends Component {
  constructor(props) {

     super(props);

     this.state = {
         isLoading: true
       }
     }

componentDidMount(){

   return fetch('http://192.168.1.253/User_Project/profile.php')
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({
              isLoading: false,
              datas: responseJson
            }, function() {
              // In this block you can do something with new state.
            });
          })
          .catch((error) => {
            console.error(error);
          });

  }

  UpdateStudentRecord = () =>{

    fetch('http://192.168.1.253/User_Project/updates_profile.php', {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({

                name: this.state.UserName,

                email: this.state.UserEmail,

                password: this.state.UserPassword
              })

              }).then((response) => response.json())
                  .then((responseJson) => {

                    // Showing response message coming from server updating records.
                    Alert.alert(responseJson);

                  }).catch((error) => {
                    console.error(error);
                  });

        }

_signOutAsync= async () => {
   await AsyncStorage.clear();
  this.props.navigation.navigate('logout', {

         })
       }


  render(){
    if (this.state.isLoading) {
        return (
          <View style={{flex: 1, paddingTop: 20}}>
            <ActivityIndicator />
          </View>
        );
      }
    return(

      <ScrollView>


      <View>
      <Text style={{fontSize: 21,
      fontWeight: '600',
      marginTop: 5,
      marginBottom: 20,
      paddingLeft: 20,
      paddingRight: 20,
      margin: 5,
      padding:32,
      backgroundColor:'#ffffff'}}>
      Profile
      </Text>
      </View>
      <View style={styles.container}>

        <FlatList
             data={ this.state.datas }
             enableEmptySections = {true}
             renderItem={({item}) =>
            <View>
            <TextInput style={styles.inputBox}
                  underlineColorAndroid='rgba(0,0,0,0)'
                  selectionColor="#fff"
                  onChangeText={name => this.setState({UserName : name})}
                  onSubmitEditing={()=> this.email.focus()}
                  value={item.name}>
                  </TextInput>
                  <TextInput style={styles.inputBox}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        selectionColor="#fff"
                        onChangeText={email => this.setState({UserEmail : email})}
                        onSubmitEditing={()=> this.email.focus()}
                        value={item.email}>
                        </TextInput>
                        <TextInput style={styles.inputBox}
                              underlineColorAndroid='rgba(0,0,0,0)'
                              selectionColor="#fff"
                              onChangeText={password => this.setState({UserPassword : password})}
                              onSubmitEditing={()=> this.email.focus()}
                              value={item.password}>
                              </TextInput>


                              </View>

        }
                    keyExtractor={(item, index) => index.toString()}

              />




              <Card style={styles.bar}>
                      <Card style={[styles.button,styles.sep]}>
                        <TouchableOpacity onPress={this.UpdateStudentRecord}><Text style={styles.btnText}>Save</Text></TouchableOpacity>

                          </Card>
                          <Card style={styles.button}>
                  <TouchableOpacity   onPress={this._signOutAsync}><Text style={styles.btnText}>SignOut</Text></TouchableOpacity>
                            </Card>
                </Card>


      </View>




      </ScrollView>

    );
  }
}
export default ProfileContainer = createStackNavigator(
    {
      Pack : { screen: ProfileContainer},
      logout: {screen: LoggedOut},

    },
    {
      initialRouteName: 'Pack',
      navigationOptions: {
        header: null,
        gesturesEnabled: true,
      },// this is needed to make sure header is hidden
    }
  );
const styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  },
inputBox: {
      width:300,
       backgroundColor:'black',
       borderRadius:40,
        paddingHorizontal:16,
       fontSize: 16,
        color:'#ffffff',
      marginVertical:10,
      height:60
    },
  bar:{
    flexDirection:'row',
  },  sep:{
      borderRightWidth:2
    },button:{
      backgroundColor:'rgba(255, 255, 255, 0.3)',
      padding:10,
      flex:1,
      alignItems:'center'
    },
    btnText:{
      color:'black',fontWeight:'bold'
    },

});
