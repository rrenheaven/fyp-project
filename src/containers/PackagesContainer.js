import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  ScrollView,
TouchableOpacity,FlatList,Alert,YellowBox
} from 'react-native';
import {
      Card,
      CardItem,
      Thumbnail,
      Body,
      Left,
      Right,Button
       } from 'native-base';
import colors from '../styles/colors';
import { createStackNavigator } from 'react-navigation';
import Image from 'react-native-scalable-image';
import Swiper from 'react-native-swiper';
import PackagesLegoCon from '../components/packages/PackagesLegoCon';
import PackagesKlcc from '../components/packages/PackagesKlccCon';

   class PackagesCon extends React.Component {
     constructor(props) {
      super(props);
        this.state = {
        }

        YellowBox.ignoreWarnings([
         'Warning: componentWillMount is deprecated',
         'Warning: componentWillReceiveProps is deprecated',
       ]);

      }
      componentDidMount(){

       return fetch('http://192.168.1.253/User_Project/bigpack_list.php')
              .then((response) => response.json())
              .then((responseJson) => {
                this.setState({
                  isLoading: false,
                  datapackCon: responseJson
                }, function() {
                  // In this block you can do something with new state.
                });
              })
              .catch((error) => {
                console.error(error);
              });

      }

      _moredetail(pack_id){
        // console.log('Add' ,this.state.pack_id);
        // console.log('get' ,packid);

        this.props.navigation.navigate('klcc', {
                 dataPack: this.state.datapackCon,//store page data,
                 packid:pack_id
               })

             }






     render() {

       return (
         <ScrollView style={styles.scrollView}>
         <Card>
         <Text style={{fontSize: 21,
         fontWeight: '600',
         marginBottom: 30,
         marginTop: 5,
         paddingLeft: 20,
         paddingRight: 20,
         margin: 5,
         padding:32,
       backgroundColor:'#ffffff'}}>
         Packages</Text>

 <FlatList
             data={ this.state.datapackCon }
             renderItem={({item}) =>
         <Card style={{margin:10,padding:10}}>
             <CardItem>
         //each compoennt need wrapper by one carditem
               <Left>
                   <Body>
                   <Text>
                   <Text style={styles.heading1}>{item.pack_name}</Text>
                   </Text>
                   <Text note>
Novemebr 23, 2018
                   </Text>
                   </Body>
               </Left>
             </CardItem>
             <Card>
               <Image
                style={styles.imageView}
                source = {{ uri: item.pack_img }} />
              </Card>
                       <CardItem>
                                   <Text style={styles.description}>
                                   <Text style={styles.heading2}>{item.pack_detail}</Text>
                                   </Text>
                       </CardItem>

                       <CardItem style={{height:50, paddingLeft:280}}>
                       <Left>
                           <Button
                             onPress={() => this._moredetail(item.pack_id)}>
                            <Text>
More Detail
                            </Text>
                         </Button>
                       </Left>
                       </CardItem>
         </Card>
                     }
 keyExtractor={(item, index) => index.toString()}

   />
         </Card>
         </ScrollView>

       );
     }
   }


   const RootStack = createStackNavigator(
     {
       Home: PackagesCon,
       lego: PackagesLegoCon,
       klcc: PackagesKlcc
     },
     {
       initialRouteName: 'Home',
       navigationOptions: {
         header: null,
         gesturesEnabled: true,
       },// this is needed to make sure header is hidden
     }
   );

   export default class App extends React.Component {
     render() {
       return <RootStack />;
     }
   }


   const styles = StyleSheet.create({
     wrapper: {
 },
     scrollView: {
       height: '100%',
     },
     MainContainer: {
       flex: 1,
       justifyContent: 'center',
       alignItems: 'center',
       padding: 10
    },
     heading: {
       fontSize: 20,
       fontWeight: '600',
       marginBottom: 30,
       marginTop: 40,
       paddingLeft: 20,
       paddingRight: 20,
     },
     description: {
       fontSize: 12,
       lineHeight: 18,
       color: colors.gray04,
       paddingLeft: 10,
       paddingRight: 10,
     },
     footer: {
     	position: 'absolute',
     	width: '100%',
     	height: 80,
     	bottom: 0,
     	borderTopWidth: 1,
     	borderTopColor: colors.gray05,
     	paddingLeft: 20,
     	paddingRight: 20,
     },
     findHomesButton: {
     	paddingTop: 15,
     	paddingBottom: 15,
     	marginTop: 16,
     	borderRadius: 3,
     	backgroundColor: colors.pink,
     },
     findHomesButtonText: {
       color: colors.white,
       textAlign: 'center',
       fontWeight: '600',
     },
     imageView: {
         width: '300%',
         height: 20 ,
         padding:10,
         borderRadius : 7,
         marginLeft:50
      },
   });
