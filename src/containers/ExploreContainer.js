import React, { Component } from 'react';
import {
  ScrollView,
  StyleSheet,
  TouchableHighlight,
  Image,
  View,
  Text,
  ImageBackground,
  TouchableOpacity
} from 'react-native';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import SearchBar from '../components/SearchBar';
// import Categories from '../components/explore/Categories';
// import Listings from '../components/explore/Listings';
import colors from '../styles/colors';
// import categoriesList from '../data/categories';
// import listings from '../data/listings';
import PlannerDatePickerkuala from '../screens/PlannerDatePickerScreenKL';
import PlannerDatePickerMelaka from '../screens/PlannerDatePickerScreenMK';
import PlannerDatePickerpenang from '../screens/PlannerDatePickerScreenPN';


import { createStackNavigator } from 'react-navigation';

class ExploreContainer extends Component {



  render() {

    return (
<View style={styles.wrapper}>
      <View>
      <Text style={{fontSize: 21,
      fontWeight: '600',
      marginTop: 5,
      marginBottom: 20,
      paddingLeft: 20,
      paddingRight: 20,
      margin: 5,
      padding:32,
    backgroundColor:'#ffffff'}}>
    Your Trips
</Text>
      </View>
      <SearchBar />


      <View style={styles.wrapper}>
        <ScrollView
          style={styles.scrollview}
          contentContainerStyle={styles.scrollViewContent}
        >
          <View style={styles.categories}>
          <TouchableHighlight
          style={styles.card}
          onPress={() => this.props.navigation.navigate('Kuala')}>
              <ImageBackground
              imageStyle={{ resizeMode: 'cover' }}
              source={require('../../src/img/klcc3.jpg')}
              style={styles.image}
            >
                <View>
                <Text style={{
                  fontSize: 20,
                fontWeight: 'bold',
                paddingLeft: 20,
                paddingTop: 20,
                color: colors.white}}>
                  KUALA LUMPUR
                </Text>
              </View>
              </ImageBackground>
          </TouchableHighlight>

          <TouchableHighlight
          style={styles.card}
          onPress={() => this.props.navigation.navigate('Melaka')}>
              <ImageBackground
              imageStyle={{ resizeMode: 'cover' }}
              source={require('../../src/img/m.jpg')}
              style={styles.image}
            >
                <View>
                <Text style={{
                  fontSize: 20,
                fontWeight: 'bold',
                paddingLeft: 20,
                paddingTop: 20,
                color: colors.white}}>
                  Melaka
                </Text>
              </View>
              </ImageBackground>
          </TouchableHighlight>

          <TouchableHighlight
          style={styles.card}
          onPress={() => this.props.navigation.navigate('Penang')}>
              <ImageBackground
              imageStyle={{ resizeMode: 'cover' }}
              source={require('../../src/img/p.jpg')}
              style={styles.image}
            >
                <View>
                <Text style={{
                  fontSize: 20,
                fontWeight: 'bold',
                paddingLeft: 20,
                paddingTop: 20,
                color: colors.white}}>
                  Penang
                </Text>
              </View>
              </ImageBackground>
          </TouchableHighlight>

          </View>
        </ScrollView>
      </View>
      </View>

    );
  }
}

const Root = createStackNavigator(
  {
    Home: ExploreContainer,
    Kuala: PlannerDatePickerkuala,
    Melaka: PlannerDatePickerMelaka,
    Penang: PlannerDatePickerpenang,


  },
  {
    initialRouteName: 'Home',
    navigationOptions: {
      header: null,
      gesturesEnabled: true,
    },// this is needed to make sure header is hidden
  }
);

export default class ExploreContainerTab extends React.Component {
  render() {
    return <Root />;
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.white,
  },
  scrollview: {
    // paddingTop: 100,
  },
  scrollViewContent: {
    paddingBottom: 80,
  },
  categories: {
    marginBottom: 40,
  },
  heading: {
    fontSize: 22,
    fontWeight: '600',
    paddingLeft: 20,
    paddingBottom: 20,
    color: colors.gray04,
  },
  image: {
    flex: 1,
    width: undefined,
    height: 180,
    padding:10,
    justifyContent:'space-between',

  },
  card: {
    display: 'flex',
    flexDirection: 'row',
    width: 500,
    justifyContent:'space-between',
    padding:10,
    height: 200,


  },
});
