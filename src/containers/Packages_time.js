import React, { Component } from 'react';
import {
  ScrollView,
  StyleSheet,
  TouchableHighlight,
  Image,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  StatusBarIOS,
    PixelRatio,Alert
} from 'react-native';
import { Card, CardItem,
   Body, Left, Right, Button } from 'native-base';
import colors from '../styles/colors';
import { createStackNavigator } from 'react-navigation';
import PlaceArea from '../screens/PlaceArea';
import DateTimePicker from 'react-native-datepicker'
import PackagesKlcc from '../components/packages/PackagesKlccCon'

class Packages_time extends React.Component {

  constructor(props) {
   super(props);
     this.state = {
       isLoading: true,
       enddate: '',
       startdate:'',
       packid:''
     }
   }
   async componentDidMount() {
            const packid = await this.props.navigation.getParam('packid');


            console.log('id' ,packid);

        }

   _next = () => {

                 const {navigation} = this.props
                  const {startdate, enddate} = this.state
                  const isDateEmpty = (startdate === "") || (enddate === "")

                  if(isDateEmpty) return Alert.alert(
                        'Alert',
                        'Please Choose the Date and Place',
                        [{text: '', onPress: () => console.log('OK Pressed')},],
                        { cancelable: false }
                    )

                    const packid = navigation.getParam("packid", "");
                    const dataPack =  navigation.getParam('dataPack');

                  return navigation.navigate('PackagesKlcc', {
                      startdate,
                      enddate,
                      packid,
                      dataPack
                  })

              }

  render() {

    // if (this.state.isLoading) {
    //   return (
    //     <View style={{flex: 1, paddingTop: 20}}>
    //       <ActivityIndicator />
    //     </View>
    //   );
    // }
    //
    // const {dataplan} = this.state
    // if(dataplan.length === 0) return <Text>Downloading Data</Text>
    return (
<View style={styles.wrapper}>
      <View>
      <Text style={styles.title}>Your Trips</Text>
      </View>
      <View style={styles.wrapper}>
        <ScrollView
          style={styles.scrollview}
          contentContainerStyle={styles.scrollViewContent}
        >
          <Card>
          <CardItem>
          <TouchableOpacity onPress={this._showDateTimePicker}>
        </TouchableOpacity>
           <Text>Depart   :</Text>
             <DateTimePicker
               style={{width: 200}}
                 date={this.state.startdate}
                 placeholder="select Date"
                   mode="date"
                    format="YYYY-MM-DD"
                    minDate="2019-02-15"
                    maxDate="2019-02-16"
                      confirmBtnText="Confirm"
                       cancelBtnText="Cancel"
                       customStyles={{
                         dateIcon: {
                         position: 'absolute',
                         left: 0,
                         top: 4,
                         marginLeft: 0
                          },
                      dateInput: {
                    marginLeft: 36
                  }
                }}
            onDateChange={(datetime) => {this.setState({startdate: datetime})}}
           />
          </CardItem>

          <CardItem>
          <TouchableOpacity onPress={this._showDateTimePicker}>
        </TouchableOpacity>
           <Text>End Date   :</Text>
             <DateTimePicker
               style={{width: 200}}
                 date={this.state.enddate}
                 placeholder="select End Date"
                   mode="date"
                    format="YYYY-MM-DD"
                    minDate="2019-02-15"
                    maxDate="2019-02-16"
                      confirmBtnText="Confirm"
                       cancelBtnText="Cancel"
                       customStyles={{
                         dateIcon: {
                         position: 'absolute',
                         left: 0,
                         top: 4,
                         marginLeft: 0
                          },
                      dateInput: {
                    marginLeft: 36
                  }
                }}
            onDateChange={(datetime1) => {this.setState({enddate: datetime1})}}
           />
          </CardItem>
          <Button
            style={{marginLeft:130,padding:50}}
            onPress={() => this._next()}>
            <Text style={styles.btnText}>Next</Text>
          </Button>
          </Card>

        </ScrollView>
      </View>
      </View>

    );
  }
}


export default Packages_time;


const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.white,
  },
  title:
  {
    fontSize: 21,
    fontWeight: '600',
    marginBottom: 30,
    marginTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    margin: 5,
    padding:32,
  backgroundColor:'#ffffff'
},
smalltitle:{
  fontSize: 20,
  fontWeight: 'bold',
  paddingLeft: 20,
  paddingTop: 20,
  color: colors.white
},
  scrollview: {
    // paddingTop: 100,
  },
  scrollViewContent: {
    paddingBottom: 80,
  },
  categories: {
    marginBottom: 40,
  },
  heading: {
    fontSize: 22,
    fontWeight: '600',
    paddingLeft: 20,
    paddingBottom: 20,
    color: colors.gray04,
  },
  image: {
    flex: 1,
    width: undefined,
    height: 180,
    padding:10,
    justifyContent:'space-between',

  },
  card: {
    display: 'flex',
    flexDirection: 'row',
    width: 500,
    justifyContent:'space-between',
    padding:10,
    height: 200,
  },
  container: {
         flex: 1,
         backgroundColor: '#fff',
         justifyContent: 'center',
         paddingHorizontal: 10,
         margin:15
     },
});
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingTop: 13,
        paddingHorizontal: 10,
        paddingBottom: 12,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: 'white',
        color: 'black',
    },
    inputAndroid: {
        fontSize: 16,
        paddingTop: 13,
        paddingHorizontal: 10,
        paddingBottom: 12,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: 'white',
        color: 'black',
    },
});
