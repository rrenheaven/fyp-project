export const addObjToPac = (pack) => {
	const m = pack.map(obj => ({
    name: obj.place_name,
    time: '15:00'
  }))
  return {
  	type: 'ADD_TO_PAC',
    payload: m
  }
}

export const changePackTimeToSchedule = (timeA,index) => {
  console.log(timeA)
    return {
        type: 'CHANGE_PACK_TIME',
        payload: {
            attTime: timeA,
            index: index
        }
    }
}

export const remove = () => {
    return {
        type: 'REMOVE_STATE',

    }
}

const initalState ={
  pack:[]

}
export const packages = (state = initalState,action) => {
    switch(action.type){


      case 'ADD_TO_PAC':{
        return {...state, pack: [...state.pack,...action.payload]}

              }

      case 'CHANGE_PACK_TIME':{
        const {attTime, index} = action.payload
        state.pack[index].attTime = attTime
        return state

                    }
                    case 'REMOVE_STATE':{
                      return {...state,pack:[]}
                          }
    }
    return state;
}
