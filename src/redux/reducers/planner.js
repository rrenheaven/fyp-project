export const addObjToRes = (obj) => {
    return {
        type: 'ADD_TO_RES',
        payload: {
            obj
        }
    }
}

export const removeObjToRES = (res_id) => {
    return {
        type: 'REMOVE_FROM_RES',
        payload: {
            res_id
        }
    }
}
const initalState = []

export const restaurant = (state = initalState, action) => {
    switch(action.type){
        case 'ADD_TO_RES' :
            const isExist = state.find(curr => curr === action.payload.obj.res_name)
            if(isExist) return state
            return [...state, action.payload.obj.res_name]
        case 'REMOVE_FROM_RES':
        return state.filter(curr => curr !== action.payload.res_id)
    }
    return state;
}
