

export const addObjToPlan = (obj) => {
    return {
        type: 'ADD_TO_PLANLIST',
        payload: {
            obj
        }
    }
}

export const removeObjFromPlan = (hotel_id) => {
  console.log('hereid',hotel_id)
    return {
        type: 'REMOVE_FROM_PLANLIST',
        payload: {
            hotel_id
        }
    }
}

const initalState = []
export const hotel = (state = initalState, action) => {
    switch(action.type){
        case 'ADD_TO_PLANLIST' :
           const isExist = state.find(curr => curr === action.payload.obj.hotel_name)
           if(isExist) return state
           return [...state, action.payload.obj.hotel_name]

        case 'REMOVE_FROM_PLANLIST':
            return state.filter(curr => curr !== action.payload.hotel_id)
            }
    return state;
}
