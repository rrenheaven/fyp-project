
export const addobj = (dataplan) => {
    return {
        type: 'ADD_OBJ',
        payload: {
          pname:dataplan.place_name
        }
    }
}
export const addTimeHotel = (hotel) => {
    return {
        type: 'ADD_HOTEL',
        payload: {
          name: hotel.place_name,
            attTime:'15:00'
        }
    }
}

export const removeHotelToSchedule = (index) => {
    return {
        type: 'REMOVE_HOTEL',
        payload: {
            index: index
        }
    }
}


export const changeHotelAttTimeToSchedule = (time,index) => {
    return {
        type: 'CHANGE_HOTEL_TIME',
        payload: {
            attTime: time,
            index: index
        }
    }
}

export const removestate = () => {
    return {
        type: 'REMOVE_STATE',

    }
}


const initalState = {
    hotel: [],
    obj:[]
}
export const time = (state = initalState, action) => {
    switch(action.type){

      case 'ADD_OBJ':{

                  return {...state, obj: [...state.obj, action.payload]}
              }

        case 'ADD_HOTEL':{
                    const {name} = action.payload
                    const isExist = state.hotel.find(curr => curr.name === name)
                    if(isExist) return state
                    return {...state, hotel: [...state.hotel, action.payload]}
                }

        case 'REMOVE_HOTEL':{
                  const {index} = action.payload
                  const newHotel = state.hotel.filter((curr, currIndex) => currIndex !== index)
                  return {...state, hotel: newHotel}
              }

        case 'CHANGE_HOTEL_TIME':{
                  const {attTime, index} = action.payload
                  state.hotel[index].attTime = attTime
                  return state
              }

              case 'REMOVE_STATE':{
                return {...state,hotel:[]}
                    }
            }
    return state;
}
