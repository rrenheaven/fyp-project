import thunk from 'redux-thunk';
import { combineReducers, createStore, applyMiddleware } from 'redux';

// import {auth} from './reducers/plan'
// import {hotel} from './reducers/plan'
import {packages} from './reducers/packages'
import {time} from './reducers/time'
import {auth} from './reducers/auth'

const reducers =  combineReducers({
    packages,
    time,
    auth,

})

const middleware = [thunk];
const store = createStore(reducers, applyMiddleware(...middleware));

export default store;
