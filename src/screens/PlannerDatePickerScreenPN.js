import React from 'react';
import { View, Text, StyleSheet,ScrollView,AsyncStorage, Image
  ,TouchableOpacity,Modal,TouchableHighlight,ImageBackground} from "react-native";
import moment from 'moment';
import DatePicker from 'react-native-datepicker'
import Icon from '@expo/vector-icons/FontAwesome';
import { Card, CardItem,
   Body, Left, Right, Button } from 'native-base';
class PlannerDatePickerPN extends React.Component{
signOut = async()=>{
  this.props.navigation.navigate('PlanS')
}
backHome = async()=>{
  this.props.navigation.navigate('Home')
}
constructor(props){
  super(props)
  this.state = {date:"2016-05-15"}
  this.state = {
   pickerSelection: 'DESTINATION',
   pickerDisplayed: false
 }
}
setPickerValue(newValue) {
   this.setState({
     pickerSelection: newValue
   })

   this.togglePicker();
 }

 togglePicker() {
   this.setState({
     pickerDisplayed: !this.state.pickerDisplayed
   })
 }
  render(){

    return(
      <View style={styles.container}>
        <Card style={{margin:10}}>
  <ScrollView style={{margin:10}}>
                <Card>
                <Left>
                <Text style={{fontSize: 21,
                fontWeight: '600',
                marginBottom: 30,
                marginTop: 5,
                paddingLeft: 20,
                paddingRight: 20,
                margin: 5,
                padding:28,
              backgroundColor:'#ffffff'}}>
                Plan</Text>                </Left>
                </Card>
                <CardItem>
                <ImageBackground
                  style={{
                    alignSelf: 'center',
                    height: 150,
                    width: 400,
                  }}
                  source={require('../../src/img/p.jpg')}
                  resizeMode="cover"
                >
                <View>
                
              </View>
              </ImageBackground>

                </CardItem>
                <Card style={{margin:10,padding:10}}>
                      <CardItem>
                        <Left>
                        <Body styele={{padding:80}}>
                                <Text style={styles.welcome}>Destination:  </Text>
                                <TouchableHighlight style={styles.des}>
                                  <Text style={{
                                    fontSize: 25,
                                  fontWeight: 'bold',
                                  }}>Penang</Text>
                                </TouchableHighlight>

                        </Body>
                        </Left>
                      </CardItem>
                    </Card>



                      <CardItem>
                      <Text>Depart   :</Text>
                                  <DatePicker
                                    style={{width: 200}}
                                    date={this.state.date}
                                    mode="date"
                                    format="YYYY-MM-DD "
                                    minDate="2018-9-25"
                                    maxDate="2018-09-30"
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    customStyles={{
                                      dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,
                                        marginLeft: 0
                                      },
                                      dateInput: {
                                        marginLeft: 36
                                      }
                                      // ... You can check the source to find the other keys.
                                    }}
                                    onDateChange={(date) => {this.setState({date: date});}}
                                  />
                      </CardItem>
                        <CardItem>
                            <Text>End       : </Text>
                               <DatePicker
                                 style={{width: 200}}
                                 date={this.state.date1}
                                 mode="date"
                                 format="YYYY-MM-DD"
                                 confirmBtnText="Confirm"
                                 cancelBtnText="Cancel"
                                   minDate="2018-9-25"
                                   maxDate="2018-09-30"
                                 customStyles={{
                                   dateIcon: {
                                     position: 'absolute',
                                     left: 0,
                                     top: 4,
                                     marginLeft: 0
                                   },
                                   dateInput: {
                                     marginLeft: 36
                                   }
                                 }}
                                 minuteInterval={10}
                                 onDateChange={(date) => {this.setState({date1: date});}}
                               />
                        </CardItem>

                  <Card style={styles.bar}>

                      <Card style={[styles.button,styles.sep]}>
                      <TouchableOpacity onPress={this.backHome}><Text style={styles.btnText}>Back</Text></TouchableOpacity>
                      </Card>
                      <Card style={styles.button}>
                      <TouchableOpacity onPress={this.signOut}><Text style={styles.btnText}>Next</Text></TouchableOpacity>
                      </Card>

                  </Card>

                  </ScrollView>
        </Card>
      </View>


    );
  }
}
export default PlannerDatePickerPN;
const styles = StyleSheet.create({
  bar:{
    flexDirection:'row',
  },
  sep:{
    borderRightWidth:2
  },
  container:{
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',

  },
  des:{
    alignItems:'center',
    margin:20,
    fontSize:20,
    color:'red'
  },
  button:{
    backgroundColor:'rgba(255, 255, 255, 0.3)',
    padding:18,
    flex:1,
    alignItems:'center'
  },
  btnText:{
    color: 'black',
     fontWeight: '500',
    fontSize: 20,
    alignItems: 'center',
    justifyContent: 'center',
   },


});
