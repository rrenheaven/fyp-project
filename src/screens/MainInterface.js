import { createStackNavigator } from 'react-navigation';
import React, { Component } from 'react';

import PlannerDatePickerkl from '../screens/Datepicker';
import PlaceArea from '../screens/PlaceArea';

const MainInterfaceRoute = createStackNavigator(
  {
    PlannerDatePickerkl: {screen: PlannerDatePickerkl},
    PlaceArea: {screen: PlaceArea},


  },
  {
    initialRouteName: 'PlaceArea',
    navigationOptions: {
      header: null,
      gesturesEnabled: true,
    },// this is needed to make sure header is hidden
  }
);


class Interface extends React.Component {
render(){
  return <MainInterfaceRoute/>
}

};
export default MainInterfaceRoute;
