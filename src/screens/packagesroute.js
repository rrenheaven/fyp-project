import { createStackNavigator } from 'react-navigation';
import React, { Component } from 'react';

import PackagesKlcc from '../components/packages/PackagesKlccCon'
import PackContainer from '../containers/PackagesContainer';
import PackTIme from '../screens/packtime';
import Packages_time from '../containers/Packages_time';

const PRoute = createStackNavigator(
  {
    PackContainer: {screen: PackContainer},
    PackagesKlcc: {screen: PackagesKlcc},
    PackTIme: {screen: PackTIme},
    Packages_time: {screen: Packages_time},


  },
  {
    initialRouteName: 'PackContainer',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
      // tabBarBottomVisible: false
    },// this is needed to make sure header is hidden
  }
);


class PackInterfaceRoute extends React.Component {
render(){
  return <PRoute/>
}

};
export default PRoute;
