import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import {createSwitchNavigator,
   createStackNavigator,
   createDrawerNavigator,
   createBottomTabNavigator
} from "react-navigation";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ActionCreators from '../redux/actions';
import colors from '../styles/colors';
import transparentHeaderStyle from '../styles/navigation';
import InputField from '../components/form/InputField';
import NextArrowButton from '../components/buttons/NextArrowButton';
import Notification from '../components/Notification';
import Loader from '../components/Loader';
import NavBarButton from '../components/buttons/NavBarButtons';
import LogIn from "../screens/LogIn"

import {
    Text,
    View,
    StyleSheet,
    ScrollView,
    KeyboardAvoidingView,TextInput,TouchableOpacity,Alert,AsyncStorage,
} from 'react-native';


class SignUpScreen extends React.Component{
  signIn = async () =>{
    await AsyncStorage.setItem('userToken', 'derren')

    this.props.navigation.navigate('Second')
  }
  signup =async()=>{
    this.props.navigation.navigate('Second')
  }
  constructor() {

    super()

    this.state = {

      UserName: '',
      UserEmail: '',
      UserPassword: ''

    }

  }

UserRegistrationFunction = () =>{

  fetch('http://192.168.1.253/User_Project/user_registration.php', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({

      name: this.state.UserName,

      email: this.state.UserEmail,

      password: this.state.UserPassword

    })

  }).then((response) => response.json())
        .then((responseJson) => {

          // If server response message same as Data Matched
         if(responseJson === 'Data Matched')
          {

              //Then open Profile activity and send user email to profile activity.
              this.props.navigation.navigate('LogIn');

          }
          else{

            Alert.alert(responseJson);
          }

        }).catch((error) => {
          console.error(error);
        });


      }


  render(){
    return(
      <View style={styles.container}>


  <View style={styles.content}>

      <TextInput style={styles.inputBox}
            underlineColorAndroid='rgba(0,0,0,0)'
            placeholder="Enter User Name"
          onChangeText={name => this.setState({UserName : name})}
            selectionColor="#fff"
            onSubmitEditing={()=> this.email.focus()}
            />
            <TextInput style={styles.inputBox}
                  underlineColorAndroid='rgba(0,0,0,0)'
                  placeholder="Enter User Email"
          onChangeText={email => this.setState({UserEmail : email})}
                  selectionColor="#fff"
                  keyboardtype="email-address"
                    ref={(input) => this.email = input}
                  onSubmitEditing={()=> this.password.focus()}
                  />
              <TextInput style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder="Enter User Password"
            onChangeText={password => this.setState({UserPassword : password})}
                    secureTextEntry={true}
                    ref={(input) => this.password = input}
                    />
                    <TouchableOpacity style={styles.button}
                     onPress={this.UserRegistrationFunction}>
                    <Text style={styles.btnText}
                >Sign Up</Text>
                    </TouchableOpacity>
                    <View style={styles.signupTextCont}>
                        <Text style={styles.btnTextSignup}>Already have Account? </Text>
                          // {this.goBack}
                        <TouchableOpacity onPress={this.signup}>
                              <Text style={styles.btnSignup}>Sign In</Text>
                        </TouchableOpacity>
    </View>
      </View>  </View>
    )
  }
};

export default MainProject1 = createStackNavigator(
{
   First: { screen: SignUpScreen },

   Second: { screen: LogIn }

});
const styles = StyleSheet.create({
  content:{
    marginTop:250
  },
container: {
        flexGrow:1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#455a64',
        paddingHorizontal:16,
},
inputBox: {
    width:300,
     backgroundColor:'rgba(255, 255, 255, 0.2)',
     borderRadius:40,
      paddingHorizontal:16,
     fontSize: 16,
      color:'#ffffff',
    marginVertical:10,
    height:60

  },
  button:{
    width:300,
    backgroundColor:'rgba(255, 255, 255, 0.3)',
    borderRadius:25,
    paddingVertical:13,
    marginVertical:10
  },
  btnText:{
    color: '#ffffff',
     fontWeight: '500',
    fontSize: 16,
    textAlign:'center'
   },
   btnTextSignup:{
     color: 'black',
     fontSize: 15,

   },
   btnSignup:{

       color: 'white',
       fontSize: 15,

   },
   signupTextCont:{
     flexGrow:1,
     alignItems:'flex-end',
     justifyContent:'center',
     paddingVertical:20,
     flexDirection:'row'
   }
 });
