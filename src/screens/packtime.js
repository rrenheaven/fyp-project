import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  ScrollView,
TouchableOpacity,FlatList,Alert,YellowBox

} from 'react-native';
import colors from '../styles/colors';
import { Card, CardItem, Thumbnail,
   Body, Left, Right, Button } from 'native-base';
   import RoundedButton from '../components/buttons/RoundedButton';
   import { createStackNavigator } from 'react-navigation';
   import Image from 'react-native-scalable-image';
   import DateTimePicker from 'react-native-datepicker'
   import {connect} from 'react-redux';
   import {bindActionCreators} from 'redux';
   import {changePackTimeToSchedule} from '../redux/reducers/packages'
   import {remove} from '../redux/reducers/packages'


class PackTIme extends Component {
  constructor(props){
      super(props)
      this.state = {
          packid: '',
          dataklcc: [],
          detailPack:'',
          attTime:[],
          pack:[],
          packages:[]

      }
  }

  Insert_Data_Into_MySQL = (name,time) =>
  {
     var data = {
       User:this.props.auth.UserName,
         Place_Name : name,
         Time_Att :time,
         packid:this.props.navigation.state.params.packid,
         s_date:this.props.navigation.state.params.startdate,
         e_date:this.props.navigation.state.params.enddate,
         plan_id:this.props.auth.datapackid

     };
     console.log(data);
                        this.setState({ ActivityIndicator_Loading : true }, () =>
                        {
                            fetch('http://172.20.10.5/User_Project/packages_List.php',
                            {
                                method: 'POST',
                                headers:
                                {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(data)
                            }).then((response) => response.json()).then((responseJsonFromServer) =>
                            {
                                alert(responseJsonFromServer);
                                this.setState({ ActivityIndicator_Loading : false,
                                  });
                            }).catch((error) =>
                            {
                                console.error(error);
                                this.setState({ ActivityIndicator_Loading : false});
                            });
                        });
                    }


  render() {
    const { navigation } = this.props;
    const dataPack = navigation.getParam('dataPack', '');
    const packid = navigation.getParam('packid', '');
    const startdate = navigation.getParam("startdate", "");
    const enddate = navigation.getParam("enddate", "");
  	return (
      <ScrollView style={styles.scrollView}>
      <Text style={styles.text}>Packages</Text>

          <Card style={{margin:10,padding:10}}>
              {
                  // .map is to extract every single obj out from array
                  this.props.packages.pack.map((obj, index) => {
                      return (
                          <View>
                              // <Card><Text style={styles.text}>{obj.type}</Text></Card>
                              <Card>
                            <View style={{flex:1, flexDirection: 'row'}}>
                            <Text style={styles.textname}>{obj.name}</Text>
                              <View style={styles.visit}>
                               <DateTimePicker
                                 format="HH:mm"
                                 mode = 'time'
                                 date = {this.props.packages.pack.attTime}
                                 // date={this.state.hotelTime}
                                 showIcon = {false}
                                 onDateChange = {(value)=> this.props.changePackTimeToSchedule(value,index)}
                                 // onDateChange={(hotelTime) => {this.setState({hotelTime: hotelTime})}}
                              />
                              </View>
                              </View>
                              </Card>
                          </View>
                      )
                  })
              }
          </Card>
          <View>
          <Card style={{height:20, paddingLeft:280}}>
          <Left>
          <Button
          onPress = {
                    () => {
                      const name = this.props.packages.pack.map( x => x.name)
                      const time = this.props.packages.pack.map( x => x.time)
                      this.Insert_Data_Into_MySQL(name,time);
                      this.props.remove();
                      navigation.goBack(null);
                   }
                 }
          >
          <Text>
          Add to planner
          </Text>
          </Button>
          </Left>
          </Card>
          </View>
      </ScrollView>

  )
}
}

function mapStateToProps(state){
 return{

    auth:state.auth,
    packages:state.packages,
    pack:state.pack

  }
}
function mapDispatchToProps(dispatch){
    return bindActionCreators({
      changePackTimeToSchedule,
      remove
    }, dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(PackTIme);


const styles = StyleSheet.create({
  wrapper: {
     height:80,
      width:220 ,
      borderRadius : 7
},
text:{
  fontSize: 19,
  fontWeight: '600',
  margin: 5,
  padding:12,
backgroundColor:'#ffffff'
},
textname:{
  textAlignVertical:'center',
  width:'50%',
  padding:20
},
visit:{
  textAlignVertical:'center',
    width:'50%',
    padding:20
  },
  MainContainer: {
    justifyContent: 'center',
   flex:1,
   margin: 10,
 },
 context:{
   textAlignVertical:'center',
flex:8,
margin:10
},
imageView: {
    width: 20,
    height: 25 ,
    margin: 7,
    borderRadius : 7,
    marginLeft:60
 },
  heading: {
    fontSize: 20,
    fontWeight: '500',
    marginBottom: 30,
    marginTop: 40,
    paddingLeft: 20,
    paddingRight: 20,
  },
   heading2: {
      fontSize: 12,
      fontWeight: '500',
      color: '#000'
    },
  description: {
    fontSize: 12,
    lineHeight: 18,
    color: colors.gray04,
    paddingLeft: 10,
    paddingRight: 10,
  },
  footer: {
   position: 'absolute',
   width: '100%',
   height: 80,
   bottom: 0,
   borderTopWidth: 1,
   borderTopColor: colors.gray05,
   paddingLeft: 20,
   paddingRight: 20,
  },
  findHomesButton: {
   paddingTop: 15,
   paddingBottom: 15,
   marginTop: 16,
   borderRadius: 3,
   backgroundColor: colors.pink,
  },
  findHomesButtonText: {
    color: colors.white,
    textAlign: 'center',
    fontWeight: '600',
  },
});
