import React from 'react';
import { View, Text, StyleSheet,AsyncStorage,Alert,FlatList,ActivityIndicator,RefreshControl,ScrollView,Image,Modal,TouchableHighlight,TouchableOpacity,YellowBox} from "react-native";
import { Card, CardItem, Thumbnail,
   Body, Left, Right, Button } from 'native-base';
import Icon from '@expo/vector-icons/FontAwesome';
import { createStackNavigator } from 'react-navigation';
import ListInterfaceRoute from '../screens/InterFaceRoute';

import ListInterface from '../screens/ListInterface';

class PlaceArea extends React.Component {
  constructor(props){
      super(props)
      this.state = {
          place: '',
          enddate: '',
          startdate:'',
          area:'',

      }
  }

  async componentDidMount() {
    const place = await this.props.navigation.getParam('place');
    const startdate = await this.props.navigation.getParam('startdate');
    const enddate = await this.props.navigation.getParam('enddate');
      this.fetchObj({place})
      console.log('a' ,place)

  }

// server part, can be do it later
fetchObj = async ({place}) => {
    const api = 'http://172.20.10.5/User_Project/area_place.php?place=' + place
    const method = 'POST'
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
    }
    const response = await fetch(api, {method, headers});//Fetch the resource
    // console.log(response)
    const text = await response.text();//Parse it as text
    // console.log(text)
    const data = JSON.parse(text)//Try to parse it as json
    // console.log(data())
    return this.setState({
        isLoading: false,
        area: data,
    })
    // console.log('Get', this.state.dataklcc)
}

_addPlanner(list_id,name) {
  // console.log('id' ,list_id);
  // const {area} = this.state
  const {navigation} = this.props
  // const isHotelEmpty = (area) === ""

  // if(isHotelEmpty) return Alert.alert(
  //       'Alert',
  //       'Please Choose At least One Area!',
  //       [{text: '', onPress: () => console.log('OK Pressed')},],
  //       { cancelable: false }
  //   )

  const startdate = navigation.getParam("startdate", "");
  const enddate = navigation.getParam("enddate", "");
  const place = navigation.getParam('place');
  return navigation.navigate('Area', {
    list_id,
      place,
      startdate,
      enddate,
      name

  })
}

  render() {
    console.log(this.state.area);
    const {area} = this.state
    if(area.length === 0) return <Text>Downloading Data</Text>
  	return (
      <ScrollView style={styles.scrollView}>
      <Text style={styles.text}>Place</Text>

          <Card style={{margin:10,padding:10}}>
              {
                  // .map is to extract every single obj out from array
                  this.state.area.map((obj, index) => {
                      return (
                          <View>
                              <Text style={styles.text}>{obj.name}</Text>
                              <Text style={styles.description}>{obj.location}</Text>
                              <Card>
                                <Image source = {{uri: obj.list_pic}} style={styles.imageView} />
                                <Text style={styles.description}>{obj.des}</Text>
                                <CardItem style={{height:50, paddingLeft:280}}>
                               <Left>
                               <Button onPress={() => this._addPlanner(obj.list_id,obj.name)}>
                               <Text>
                               More Detail
                              </Text>
                               </Button>
                               </Left>
                               </CardItem>
                                </Card>

                          </View>
                      )
                  })
              }

          </Card>
      </ScrollView>

  )
}
}


const Area = ({area_name, area_detail, area_img}) => {

// each component should have it own style sheet
const style = StyleSheet.create({
  heading1: {
    fontSize: 15,
    fontWeight: '500',
    marginBottom: 30,
    marginTop: 40,
    paddingLeft: 20,
    paddingRight: 20,
  },
  description: {
    fontSize: 15,
    fontWeight: '600',
  }
})

return (
  <CardItem>
      <Left>
          <Body>
              <Text>
                  <Text style={style.heading1}>{area_name}</Text>
              </Text>
              <Text style={styles.imageView}>{area_img}</Text>
              <Text style={styles.description}>{area_detail}</Text>

          </Body>
      </Left>
  </CardItem>
)
}


// const Resturants = ({res_name, res_detail}) => {
//   const style = StyleSheet.create({
//     heading: {
//         fontSize: 18,
//
//     },
//     price: {
//         color: '#222222'
//     }
//   })
// return (
//   <Card>
//       <Text style={style.heading1}>{res_name}</Text>
//       <Text style={styles.description}>{res_detail}</Text>
//   </Card>
// )
// }
//
//
// const Shop = ({att_name, att_detail}) => {
//   const style = StyleSheet.create({
//     heading: {
//         fontSize: 18,
//
//     },
//     price: {
//         color: '#222222'
//     }
//   })
// return (
//   <CardItem>
//       <Text>
//           <Text style={style.heading1}>{att_name}</Text>
//       </Text>
//       <Text style={styles.description}>{att_detail}</Text>
//   </CardItem>
// )
// }

const DetailButton = ({ButtonLabel, onPress}) => {
return (
  <CardItem style={{height:50, paddingLeft:280}}>
      <Left>
          <Button onPress={() => _addPlanner()}>
              <Text>{ButtonLabel}</Text>
          </Button>
      </Left>
  </CardItem>
)
}


export default App = createStackNavigator(
{
Pack : { screen: PlaceArea},
Area: {screen: ListInterfaceRoute},
},{
initialRouteName: 'Pack',
navigationOptions: {
header: null,
gesturesEnabled: true,
},// this is needed to make sure header is hidden
}
);

const styles = StyleSheet.create({
  wrapper: {
     height:80,
      width:220 ,
      borderRadius : 7
},
text:{
  fontSize: 21,
  fontWeight: '600',
  marginBottom: 30,
  marginTop: 5,
  paddingLeft: 20,
  paddingRight: 20,
  margin: 5,
  padding:32,
backgroundColor:'#ffffff'
},
  MainContainer: {
    justifyContent: 'center',
   flex:1,
   margin: 10,
 },
 context:{
   textAlignVertical:'center',
flex:8,
margin:10
},
imageView: {
    width: 370,
    height: 300 ,
    margin: 7,
    borderRadius : 7,
 },
  heading: {
    fontSize: 20,
    fontWeight: '500',
    marginBottom: 30,
    marginTop: 40,
    paddingLeft: 20,
    paddingRight: 20,
  },
   heading2: {
      fontSize: 12,
      fontWeight: '500',
      color: '#000'
    },
  description: {
    fontSize: 15,
    lineHeight: 18,
    paddingLeft: 10,
    paddingRight: 10,
    margin:5
  },
  footer: {
   position: 'absolute',
   width: '100%',
   height: 80,
   bottom: 0,
   borderTopWidth: 1,
   paddingLeft: 20,
   paddingRight: 20,
  },
  findHomesButton: {
   paddingTop: 15,
   paddingBottom: 15,
   marginTop: 16,
   borderRadius: 3,
  },
  findHomesButtonText: {
    textAlign: 'center',
    fontWeight: '600',
  },
});
