import React from 'react';
import { View, Text, StyleSheet,AsyncStorage,Alert,FlatList,
  AppRegistry,
  ActivityIndicator,
  RefreshControl,
  ScrollView,Image,Modal,
  TouchableHighlight,TouchableOpacity,
  YellowBox} from "react-native";
import { Card, CardItem, Thumbnail,Body, Left, Right, Button, } from 'native-base';
import Icon from '@expo/vector-icons/FontAwesome';
import PlacesResScreen from '../screens/PlacesResScreen';
import PlacesAttScreen from '../screens/PlacesAttScreen';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {addObjToPlan} from '../redux/reducers/plan'
import {addTimeHotel} from '../redux/reducers/time'


class PlaceHotelScreen extends React.Component{

  constructor(props) {
     super(props);
     this.state = {
       isLoading: true,
       refreshing: false,
      ActivityIndicator_Loading: false,
       list_id: '',
       place:'',
       hoteldata:[],
       attdata:[],
       resdata:[],
       startdate:'',
       dataHo:'',
       dataRe:'',
       dataAtt:'',
       enddate: '',
       area:'',
     }
   }

  _Addhotel(hotel){
      this.props.addObjToPlan(hotel);
      const dataHo = [...this.state.dataHo];
      dataHo.push(hotel);
      this.setState({dataHo})

    const {navigation} = this.props
      return navigation.navigate('ListInterface', {
            dataHo,
             })
    }

  async componentDidMount() {
    const list_id = await this.props.navigation.getParam('list_id');
      const place = await this.props.navigation.getParam('place');
    this.fetchObjHotel({list_id,place})


  }

  fetchObjHotel = async ({list_id, place}) => {
      const api = 'http://172.20.10.5/User_Project/plan_hotel.php?list_id=' + list_id + '&place' +place
      const method = 'POST'
      const headers = {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      }
      const response = await fetch(api, {method, headers});//Fetch the resource
      // console.log(response)
      const text = await response.text();//Parse it as text
      // console.log(text)
      const data = JSON.parse(text)//Try to parse it as json
      // console.log(data())
      return this.setState({
          isLoading: false,
          hoteldata: data,
      })
  }

      // Insert_Hotel = (place_id,place_name) =>
      //               {
      //                   this.setState({ ActivityIndicator_Loading : true }, () =>
      //                   {
      //                         fetch('http://172.20.10.5/User_Project/hotel_data.php',
      //                       {
      //                           method: 'POST',
      //                           headers:
      //                           {
      //                               'Accept': 'application/json',
      //                               'Content-Type': 'application/json',
      //                           },
      //                           body: JSON.stringify(
      //                           {
      //                             list_id:this.props.navigation.state.params.list_id,
      //                             place_id:place_id,
      //                             place_name :place_name,
      //                           })
      //                       }).then((response) => response.json()).then((responseJsonFromServer) =>
      //                       {
      //                           // alert(responseJsonFromServer);
      //                           this.setState({ ActivityIndicator_Loading : false });
      //                       }).catch((error) =>
      //                       {
      //                           console.error(error);
      //                           this.setState({ ActivityIndicator_Loading : false});
      //                       });
      //                   });
      //               }


  render(){
    const { navigation } = this.props;
    const startdate = navigation.getParam('startdate', '');
    const enddate = navigation.getParam('enddate', '');
    const planname = navigation.getParam('planname', '');
    const name = navigation.getParam("place", "");
    const {hoteldata}= this.state
    return(
            <ScrollView
                refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }>
              <Card>
                <Text style={styles.title}>Hotel</Text>
                  <ScrollView style={styles.scrollView}>
                  {
                    hoteldata.length === 0 ? <Text>Downloading</Text>:
                      // .map is to extract every single obj out from array
                      this.state.hoteldata.map((hotel, index) => {
                          return (
                              <View>
                              <Card style={{margin:2,padding:2}}>
                           //
                                      <CardItem>  //each compoennt need wrapper by one carditem
                                         <Left>
                                           <Body>
                                             <Text>
                                               <Text style={styles.heading1}>{hotel.place_name}</Text>
                                                 </Text>
                                                 <Text note>
                                                      Novemebr 23, 2018
                                                 </Text>
                                                 <Text style={styles.description}>
                                                 <Text style={styles.heading2}>{hotel.des}</Text>
                                                 <Text style={styles.heading2}>{hotel.url}</Text>

                                                 </Text>
                                             </Body>
                                         </Left>
                                        </CardItem>
                                        <CardItem style={{height:50, paddingLeft:260}}>
                                             <Left>
                                                <Button
                                                   onPress={
                                                     () =>{
                                                       this.props.addTimeHotel(hotel);
                                                      // this.Insert_Hotel(hotel.place_id,hotel.place_name)
                                                      this.props.navigation.navigate('ListInterface')
                                                    }}>
                                                   <Text style={{padding:45}}>Add</Text>
                                                  </Button>
                                             </Left>
                                        </CardItem>
                              </Card>
                        //
                              </View>
                          )
                      })
                  }
         <CardItem style={{height:50, paddingLeft:250}}>
         <Left>
             <Button warning
             onPress = {
                       () => {
                         this.props.navigation.goBack();
                      }
                    }>
              <Text style={{padding:50}}>Back</Text>
           </Button>
         </Left>
         </CardItem>
       </ScrollView>
     </Card>
</ScrollView>
             );
           }
         }

         function mapStateToProps(state){
             return{
                 hotel: state.hotel,
                 time:state.time
             }
         }

         function mapDispatchToProps(dispatch){
             return bindActionCreators({
                 addObjToPlan,
                 addTimeHotel
             }, dispatch)
         }

export default connect(mapStateToProps, mapDispatchToProps)(PlaceHotelScreen);

const styles = StyleSheet.create({
  container:{
    flex: 1,
    alignItems:'center',
    justifyContent:'center'
  },
  title:
  {
    fontSize: 21,
    fontWeight: '600',
    marginBottom: 30,
    marginTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    margin: 5,
    padding:5,
  backgroundColor:'#ffffff'
},  bar:{
      flexDirection:'row',
    },  sep:{
        borderRightWidth:2
      },button:{
        backgroundColor:'rgba(255, 255, 255, 0.3)',
        padding:10,
        flex:1,
        alignItems:'center'
      },
      btnText:{
    		color:'black',fontWeight:'bold'
    	},
      imageView: {
          width: '100%',
          height: 250 ,
          margin: 7,
          borderRadius : 7
       },

});
