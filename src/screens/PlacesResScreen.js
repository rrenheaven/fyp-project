import React from 'react';
import { View, Text, StyleSheet,AsyncStorage,ScrollView,Image,Modal,TouchableHighlight,TouchableOpacity,FlatList,YellowBox} from "react-native";
import { Card, CardItem, Thumbnail,
   Body, Left, Right, Button } from 'native-base';
import Icon from '@expo/vector-icons/FontAwesome';
import { createStackNavigator } from 'react-navigation';
import PlacesAttScreen from '../screens/PlacesAttScreen';

import InboxContainer from '../containers/InboxContainer';

class PlacesResScreen extends React.Component{
  constructor(props) {

     super(props);

     this.state = {
       isLoading: true

     }

     YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

   }

  _AddRes(){
    console.log('Add',this.state.dataRe);

    this.setState({
      dataRe: this.state.resdata[0].res_name
    })
  }

  _Resdata(){
    console.log('start date', this.props.navigation.state.dataRe, );
    const startdate = this.props.navigation.getParam("startdate", "datetime");
    const dataHo = this.props.navigation.getParam("dataHo", "hotel");

    this.props.navigation.navigate('Inbox', {
            dataRe: this.state.dataRe,//store page data
            dataHo,
             startdate

           })


  }

  componentDidMount(){

   return fetch('http://192.168.1.253/User_Project/place_res.php')
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({
              isLoading: false,
            resdata: responseJson
            }, function() {
              // In this block you can do something with new state.
            });
          })
          .catch((error) => {
            console.error(error);
          });
          this.setState({
            dataH:
             ((typeof this.props.navigation.state.params == 'undefined')) ? '' : this.props.navigation.state.params.dataH,
          })


  }




  render(){
    console.log('Res',this.state.startdate);
    const { navigation } = this.props;
    const startdate = navigation.getParam('startdate', '');
    const dataHo = navigation.getParam('dataHo', '');

    return(
      <ScrollView>

               <Card>
               <Text style={{fontSize: 21,
               fontWeight: '600',
               marginBottom: 30,
               marginTop: 5,
               paddingLeft: 20,
               paddingRight: 20,
               margin: 5,
               padding:32,
             backgroundColor:'#ffffff'}}>
               Pick A Restaurants
              

               </Text>
               <ScrollView style={styles.scrollView}>
       <FlatList
         data={ this.state.resdata }


         renderItem={({item}) =>

               <Card style={{margin:10,padding:10}}>
                   <CardItem>
               //each compoennt need wrapper by one carditem
                     <Left>
                         <Body>

                         <Text>
                         <Text style={styles.heading1}>{item.res_name}</Text>
                         </Text>
                         <Text note>
      Novemebr 23, 2018
                         </Text>
                         </Body>
                     </Left>
                   </CardItem>
                   <Card>
                   <View>
                     <Image source = {{ uri: item.res_img }} style={styles.imageView} />
                   </View>
                    </Card>

                             <CardItem>
                                         <Text style={styles.description}>
                                         <Text style={styles.heading2}>{item.res_detail}</Text>
                                         </Text>
                             </CardItem>
                             <CardItem>
                             <Text style={styles.description}>
                             <Text style={styles.heading2}>{item.res_detail2}</Text>
                             </Text>
                             </CardItem>

                             <CardItem style={{height:50, paddingLeft:250}}>
                             <Left>
                                 <Button


                                   onPress={() => this._AddRes()}>
                                  <Text style={{padding:50}}
                                    >
                                       Add

                                  </Text>
                               </Button>

                             </Left>
                             </CardItem>


               </Card>

                           }
                   keyExtractor={(item, index) => index.toString()}

         />



         <CardItem style={{height:50, paddingLeft:250}}>
         <Left>
             <Button


               onPress={() => this._Resdata()}>
              <Text style={{padding:50}}
                >
                   Next

              </Text>
           </Button>

         </Left>
         </CardItem>

                         </ScrollView>

               </Card>
               </ScrollView>





             );
           }
         }



  export default PlacesResScreen = createStackNavigator(
    {
      Pack : { screen: PlacesResScreen},
      Inbox: {screen: PlacesAttScreen},

    },
    {
      initialRouteName: 'Pack',
      navigationOptions: {
        header: null,
        gesturesEnabled: true,
      },// this is needed to make sure header is hidden
    }
  );

  const styles = StyleSheet.create({
  container:{
    flex: 1,
    alignItems:'center',
    justifyContent:'center'
  },  bar:{
      flexDirection:'row',
    },  sep:{
        borderRightWidth:2
      },button:{
        backgroundColor:'rgba(255, 255, 255, 0.3)',
        padding:10,
        flex:1,
        alignItems:'center'
      },
      btnText:{
        color:'black',fontWeight:'bold'
      },
      imageView: {
          width: '100%',
          height: 250 ,
          margin: 7,
          borderRadius : 7
       },

  });
