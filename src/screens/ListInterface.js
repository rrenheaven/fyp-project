import React, { Component }from 'react';
import { View, Text, StyleSheet,AsyncStorage,Alert,FlatList,ImageBackground,
  AppRegistry,ActivityIndicator,RefreshControl,
  ScrollView,Image,Modal,TouchableHighlight,TouchableOpacity,YellowBox,
    Animated,
    Easing,
    Dimensions,
    Platform}
   from "react-native";
import { Card, CardItem, Thumbnail,
   Body, Left, Right, Button, } from 'native-base';
import Icon from '@expo/vector-icons/FontAwesome';
import DateTimePicker from 'react-native-datepicker'
import { createStackNavigator } from 'react-navigation';
import colors from '../styles/colors';
import { Provider } from 'react-redux';
import AccordioPlanner from '../components/plan/AccordioPlanner';
import PlacesResScreen from '../screens/PlacesResScreen';
import PlacesAttScreen from '../screens/PlacesAttScreen';
import PlaceHotelScreen from '../screens/PlaceHotelScreen';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {removeHotelToSchedule} from '../redux/reducers/time'
import {removeRestToSchedule} from '../redux/reducers/time'
import {removeAttToSchedule} from '../redux/reducers/time'
import {changeHotelAttTimeToSchedule} from '../redux/reducers/time'
import {changeRestAttTimeToSchedule} from '../redux/reducers/time'
import {changeAttAttTimeToSchedule} from '../redux/reducers/time'
import {removestate} from '../redux/reducers/time'
import {addplanid} from '../redux/reducers/auth'

console.disableYellowBox = true;
console.ignoredYellowBox = ['Warning: Each', "Warning: Failed prop type"];


class ListInterface extends React.Component{
  constructor(props) {
     super(props);
     this.state = {
       isLoading: false,
       refreshing: false,
      ActivityIndicator_Loading: false,
       area_id: '',
       planname:'',
       startdate:'',
       enddate: '',
       place:'',
       datadisplay: '',
       restaurantdata:'',
       attdata:'',
       hotel:'',
       restaurants:'',
       attraction:'',
       hotelTime:'',
       restTime:'',
       attTime:'',
       datais:''
     }
   }
   async componentDidMount() {
     await this.data_display();
     await this.Insert_plan();
   }
    _onRefresh = async () => {
        await this.data_display();

      }

   _interfaceIDHotel(){
     const {navigation} = this.props
     const list_id = navigation.getParam("list_id", "");
     const place =  navigation.getParam('place');
     return navigation.navigate('PlaceHotelScreen', {
       list_id,
       place
     })
    }

  _interfaceIDRes(){
    const {navigation} = this.props
    const list_id = navigation.getParam("list_id", "");
    const place =  navigation.getParam('place');
    return navigation.navigate('PlacesResScreen', {
      list_id,
      place
        })
    }

    _interfaceIDAtt(){
      const {navigation} = this.props
      const list_id = navigation.getParam("list_id", "");
      const place =  navigation.getParam('place');
      return navigation.navigate('PlacesAttScreen', {
        list_id,
        place
             })
      }

      Insert_plan = async () => {
                       var data = {
                         list_id:this.props.navigation.state.params.list_id,
                         list_name :this.props.navigation.state.params.name,
                         user:this.props.auth.UserName,
                       };
                    {
                        this.setState({ ActivityIndicator_Loading : true }, () =>
                        {
                              fetch('http://172.20.10.5/User_Project/hotel_data.php',
                            {
                                method: 'POST',
                                headers:
                                {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(data)
                            }).then((response) => response.json()).then((responseJsonFromServer) =>
                            {

                              this.setState({
                               dataid: responseJsonFromServer,
                               ActivityIndicator_Loading : false,

                                 })
                                 this.props.addplanid(responseJsonFromServer);

                            }).catch((error) =>
                            {
                                console.error(error);
                                this.setState({ ActivityIndicator_Loading : false});
                            });
                        });
                    }
}
      Insert_Data_Into_MySQL = (plan_name,plan_time) =>
                    {
                       var data = {
                         Place_Name :plan_name,
                         Time_Att :plan_time,
                         Start_Date_Time: this.props.navigation.state.params.startdate,
                         End_Date_Time: this.props.navigation.state.params.enddate,
                         List_ID: this.props.navigation.state.params.list_id,
                         User:this.props.auth.UserName,
                         plan_id:this.props.auth.dataid

                       };
                       console.log(data);

                        this.setState({ ActivityIndicator_Loading : true }, () =>
                        {
                              fetch('http://172.20.10.5/User_Project/planner_list.php',
                            {
                                method: 'POST',
                                headers:
                                {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(data)
                            }).then((response) => response.json()).then((responseJsonFromServer) =>
                            {
                                // alert(responseJsonFromServer);
                                this.setState({
                                   ActivityIndicator_Loading : false ,
                                     datadisplay: []
                                    });
                            }).catch((error) =>
                            {
                                console.error(error);
                                this.setState({ ActivityIndicator_Loading : false});
                            });
                        });
                    }

       data_display = async () => {
                              try {
                        const api = 'http://172.20.10.5/User_Project/data_Display.php'
                        const method = 'POST'
                        const headers = {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                        const response = await fetch(api, {method, headers});//Fetch the resource
                        const text = await response.text();//Parse it as text
                        const data = JSON.parse(text)//Try to parse it as json
                        return this.setState({
                            isLoading: false,
                            datadisplay: data,
                          })
                        } catch (err) {
                            console.log(err)
                        }
                      }


    // _removeObjFromPlan = async (plan_id) => {
    //               const api = 'http://172.20.10.5/User_Project/deleteHotel_Data.php?plan_id=' +plan_id
    //               const method = 'POST'
    //               const headers = {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',} // your header setting
    //               const {datadisplay} = this.state
    //                   try {
    //                     const response = await fetch(api, {method, headers});
    //                     this.setState({
    //                       datadisplay: datadisplay.filter(curr => curr.plan_id !== plan_id)
    //                     })
    //                   } catch (err) {
    //                     console.log(err)
    //                 }
    //             }

                _removeObjFromPlan = (plan_id) => {
                                 var dataplan = {
                                   plan_id:plan_id
                                 };
                                 console.log(dataplan);
                                  this.setState({ ActivityIndicator_Loading : true }, () =>
                                  {
                                        fetch('http://172.20.10.5/User_Project/delete_Data.php' ,
                                      {
                                          method: 'POST',
                                          headers:
                                          {
                                              'Accept': 'application/json',
                                              'Content-Type': 'application/json',
                                          },
                                          body: JSON.stringify(dataplan)
                                      }).then((response) => response.json()).then((responseJsonFromServer) =>
                                      {
                                          alert(responseJsonFromServer);
                                          this.setState({
                                        ActivityIndicator_Loading : false ,
                                          datadisplay:[]});
                                      }).catch((error) =>
                                      {
                                          console.error(error);
                                          this.setState({ ActivityIndicator_Loading : false});
                                      });
                                  });
                              }



render(){
  // console.log( this.props.time.hotel)
  console.log( this.props.auth.dataid)
// console.log(this.props.auth.UserName)
  const {datadisplay}= this.state
  const { navigation } = this.props;
  const startdate = navigation.getParam('startdate', '');
  const enddate = navigation.getParam('enddate', '');
  const planname = navigation.getParam('planname', '');
  const place = navigation.getParam("place", "");
  const name = navigation.getParam('name');
  const {time}= this.props

  return (
    <ScrollView
    refreshControl={
    <RefreshControl
      refreshing={this.state.refreshing}
      onRefresh={this._onRefresh}
    />
  }>
    <Text style={styles.title}>Trips</Text>
    <View>
    <Card>
    <Text style={styles.heading2}>Area: {JSON.stringify(place)}</Text>
    <Text style={styles.heading2}>Start Date: {JSON.stringify(startdate)}</Text>
    <Text style={styles.heading1}>End Date :{JSON.stringify(enddate)}</Text>

    </Card>
    <CardItem>
    <Text style={styles.heading3}>Please Create your plan by choosing one option from below:</Text>
    </CardItem>

    <View style={styles.contain}>
        <View style={styles.box}>
                 <Button light
                 style={{padding:60}}
                  onPress={() => this._interfaceIDHotel()}>
                    <Text style={styles.smalltitle}>Hotel</Text>
              </Button>
        </View>
        <View style={styles.box}>
             <Button light
              style={{padding:40}}
               onPress={() => this._interfaceIDRes()}>
                    <Text style={styles.smalltitle}>Resturants</Text>
              </Button>
        </View>
        <View style={styles.box}>
        <Button light
          style={{padding:40}}
          onPress={() => this._interfaceIDAtt()}>
                    <Text style={styles.smalltitle}>Attraction</Text>
              </Button>
        </View>

    </View>

    <Card>
    <ScrollView
    refreshControl={
    <RefreshControl
      refreshing={this.state.refreshing}
      onRefresh={this._onRefresh}
    />
  }>
    <View>
    <Card>
    <Text style={styles.heading2}>Place: {JSON.stringify(place)}</Text>
    <Text style={styles.heading3}>Plane Name: {JSON.stringify(name)}</Text>
    </Card>
     <View style={{marginTop:30}}>
     <View>

      </View>
           <View style={styles.container}>

                 <Card>

                 {
                   this.props.time.hotel.length === 0 ? <Text>Please Choose the Plan </Text>:

                   this.props.time.hotel.map((hotel, index) => {
                     return(
                     <View>
                           <Text style={styles.heading2}>Place:</Text>
                             <View style={{flex:1, flexDirection: 'row'}}>
                                 <Text style={styles.text}>{hotel.name}</Text>
                                 <View style={styles.visit}>
                                 <DateTimePicker
                                    format="HH:mm"
                                    mode = 'time'
                                    date = {this.props.time.hotel.attTime}
                                    // date={this.state.hotelTime}
                                    showIcon = {false}
                                    onDateChange = {(value)=> this.props.changeHotelAttTimeToSchedule(value,index)}
                                    // onDateChange={(hotelTime) => {this.setState({hotelTime: hotelTime})}}

                                />
                                 <TouchableOpacity
                                    onPress = {
                                      () => {
                                        this.props.removeHotelToSchedule(index)

                                        const plan_id = this.state.datadisplay.map( x => x.plan_id)
                                            this._removeObjFromPlan(plan_id)
                                    }}>
                                    <Text>X</Text>
                                   </TouchableOpacity>
                                   </View>
                             </View>
                             </View>
                           )
                       })
                   }
            </Card>
            </View>
                 <View style={{padding:45}}>
                 <TouchableOpacity
                           style = { styles.TouchableOpacityStyle }
                           onPress = {

                             () => {

                               const plan_name = this.props.time.hotel.map( x => x.name)
                               const plan_time = this.props.time.hotel.map( x => x.attTime)

                               this.Insert_plan();
                               this.props.removestate();
                               this.Insert_Data_Into_MySQL(plan_name, plan_time);

                               navigation.goBack(null);
                               // console.log(plan)

                          } }>
                             <Text style = { styles.TextStyle }>Save</Text>
                   </TouchableOpacity>
                  </View>
           </View>
      </View>
</ScrollView>
      </Card>
    </View>
    </ScrollView>

  );
}
}




function mapStateToProps(state){
 return{
  hotel: state.hotel,
    restaurants: state.restaurants,
    attraction: state.attraction,
    time:state.time,
    auth:state.auth,



  }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        removeHotelToSchedule,
        removeRestToSchedule,
        removeAttToSchedule,
        changeHotelAttTimeToSchedule,
        addplanid,
        removestate
    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(ListInterface);


const styles = StyleSheet.create({
  contain:{
    flexDirection:'row',
    flexWrap:'wrap',
  },
  box:{
    margin:12,
    width:180,
  },
  image: {
    height:180,

    padding:10,
    justifyContent:'space-between',
  },
  title:
  {
    fontSize: 21,
    fontWeight: '600',
    marginBottom: 30,
    marginTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    margin: 5,
    padding:32,
  backgroundColor:'#ffffff'
},
smalltitle:{
  fontSize: 18,
  fontWeight: 'bold',
  color: colors.black
},
heading: {
  fontSize: 22,
  fontWeight: '600',
  paddingLeft: 20,
  paddingBottom: 20,
  color: colors.gray04,
},
text:{
  textAlignVertical:'center',
  width:'50%',
  padding:20
},
container:{
  justifyContent: 'center',
flex:1,
margin: 5,
paddingTop: (Platform.OS === 'ios') ? 20 : 0,

},
visit:{
  textAlignVertical:'center',
    width:'50%',
    padding:20
  },
TouchableOpacityStyle:
{
padding:10,
backgroundColor:'#009688',
margin: 20,
width: '20%',
borderRadius:40,
marginLeft: 130,
},
});
