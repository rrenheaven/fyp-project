import { createStackNavigator } from 'react-navigation';
import React, { Component } from 'react';

import PlacesResScreen from '../screens/PlacesResScreen';
import PlacesAttScreen from '../screens/PlacesAttScreen';
import PlaceHotelScreen from '../screens/PlaceHotelScreen';
import ListInterface from '../screens/ListInterface';
import InboxContainer from '../containers/InboxContainer';
import ExploreContainer from '../containers/ExploreContainer';
import LoggedOut from '../screens/LoggedOut';
import ProfileContainer from '../containers/ProfileContainer';


const ListInterfaceRoute = createStackNavigator(
  {
    ListInterface: {screen: ListInterface},
    PlacesResScreen: {screen: PlacesResScreen},
    PlacesAttScreen: {screen: PlacesAttScreen},
    PlaceHotelScreen: {screen: PlaceHotelScreen},
    InboxContainer: {screen: InboxContainer},
    LoggedOut: { screen: LoggedOut },
    ProfileContainer:{screen:ProfileContainer},

  },
  {
    initialRouteName: 'ListInterface',
    navigationOptions: {
      header: null,
      gesturesEnabled: true,
    },// this is needed to make sure header is hidden
  }
);


class Interface extends React.Component {
render(){
  return <ListInterfaceRoute/>
}

};
export default ListInterfaceRoute;
