import React, { Component } from 'react';
import colors from '../styles/colors';
import RoundedButton from '../components/buttons/RoundedButton';
import Icon from 'react-native-vector-icons/FontAwesome';
import  transparentHeaderStyle  from '../styles/navigation'
import NavBarButtons from '../components/buttons/NavBarButtons'

import {
    Text,
    View,
    Image,
    StyleSheet,
    TouchableHighlight
} from 'react-native';



export default class LoggedOut extends Component {

    static navigationOptions = ({ navigation }) => ({
        // headerRight: <NavBarButtons handleButtonPress={() => navigation.navigate('LogIn')} location="right" color={colors.white} text="Log In" />,
        headerStyle: transparentHeaderStyle,
        headerTransparent: true,
        headerTintColor: colors.white,
      });

      constructor(props, context) {
          super(props, context);
          this._onForward = this._onForward.bind(this);
        }
        _onForward() {
            let nextIndex = ++this.props.index;
            this.props.navigator.push({
              component: SignUpScreen,
              title: 'Scene ' + nextIndex,
              passProps: {index: nextIndex},
            });
          }


    // onFacebookPressed(){
    //     alert("Facebook button pressed")
    // }
    //
    // moreOptionPress(){
    //     alert("more button pressed")
    // }
    render() {
        return (
            <View style={styles.wrapper}>
                <View style={styles.welcomeWrapper}>
                    <Image
                        source={require('../img/1.png')}
                        style={styles.logo} />
                        <Text style={styles.welcomText}>Welcome to Infinity</Text>

                        <RoundedButton
                        text="LogIn"
                        textColor={colors.green01}
                        background={colors.white}
                        icon={<Icon name="sign-in" size={20} style={styles.facebookButtonIcon}/>}
          handleOnPress={() => this.props.navigation.navigate('LogIn')}>
                        </RoundedButton>

                        <RoundedButton
                        text="Create Account"
                        textColor={colors.white}
                        handleOnPress={() => this.props.navigation.navigate('Signup')}>
                         </RoundedButton>

                        <TouchableHighlight
                        style={styles.moreOptionsButton}
                        onPress={this.moreOptionPress}>
                            <Text style={styles.moreOptionsButtonText}>More Option</Text>
                        </TouchableHighlight>

                        <View style={styles.termsAndConditions}>
                            <Text style={styles.termsText}>By Tapping continue, Create account or More</Text>
                            <Text style={styles.termsText}>options, </Text>
                            <Text style={styles.termsText}>I agree to Airbnb's </Text>
                            <TouchableHighlight style={styles.linkButton}>
                                <Text style={styles.termsText}>Terms of service</Text>
                            </TouchableHighlight>

                            <Text style={styles.termsText}>, </Text>
                            <TouchableHighlight style={styles.linkButton}>
                                <Text style={styles.termsText}>Payment Terms of Service</Text>
                            </TouchableHighlight>

                            <Text style={styles.termsText}>, </Text>
                            <TouchableHighlight style={styles.linkButton}>
                                <Text style={styles.termsText}>Privacy Policy</Text>
                            </TouchableHighlight>

                            <Text style={styles.termsText}>, and </Text>
                            <TouchableHighlight style={styles.linkButton}>
                                <Text style={styles.termsText}>Nondiscrimination Policy</Text>
                            </TouchableHighlight>
                            <Text style={styles.termsText}>.</Text>
                        </View>
                </View>

            </View>
        );
    }
}


const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: colors.green01
    },
    logo: {
        width: 150,
        height: 50,
        marginTop: 50,
        marginBottom: 40
    },
    welcomeWrapper: {
        flex: 1,
        marginTop: 20,
        padding: 20
    },
    welcomText:{
        fontSize:30,
        color:colors.white,
        fontWeight:'300',
        marginBottom:40
    },
    facebookButtonIcon:{
        color:colors.green01,
        position:'relative',
        left:20,
        zIndex:8
    },
    moreOptionsButton:{
        marginTop:15
    },
    moreOptionsButtonText:{
        color:colors.white,
        fontSize:16
    },
    termsAndConditions:{
        flexWrap:'wrap',
        alignItems:'flex-start',
        flexDirection:'row',
        marginTop:30
    },
    termsText:{
        color:colors.white,
        fontSize:12,
        fontWeight:'600'
    },
    linkButton:{
        borderBottomWidth:1,
        borderBottomColor:colors.white
    }

});
