import React from 'react';
import { View, Text, StyleSheet,ScrollView,AsyncStorage, Image
  ,TouchableOpacity,Modal,TouchableHighlight,ImageBackground,AppRegistry,PanResponder} from "react-native";
import moment from 'moment';
import DatePicker from 'react-native-datepicker'
import Icon from '@expo/vector-icons/FontAwesome';
import { Card, CardItem,
   Body, Left, Right, Button } from 'native-base';
   import { createStackNavigator } from 'react-navigation';
   import PlacesHotelScreen from '../screens/PlacesHotelScreen';

class PlannerDatePickerkl extends React.Component{

  constructor(props) {
      super(props);

      this.state = {
        date: '',
        time: '20:00',
        datetime: '2016-05-05 20:00',
        datetime1: '2016-05-05 20:00',
      };
    }
    componentWillMount() {
        this._panResponder = PanResponder.create({
          onStartShouldSetPanResponder: (e) => {console.log('onStartShouldSetPanResponder'); return true;},
          onMoveShouldSetPanResponder: (e) => {console.log('onMoveShouldSetPanResponder'); return true;},
          onPanResponderGrant: (e) => console.log('onPanResponderGrant'),
          onPanResponderMove: (e) => console.log('onPanResponderMove'),
          onPanResponderRelease: (e) => console.log('onPanResponderRelease'),
          onPanResponderTerminate: (e) => console.log('onPanResponderTerminate')
        });
      }
 setStartDate(datetime1){
   this.setState({
     startdate: datetime1
   })
 }


 _onNext(){
   console.log('start date', this.state.startdate, 'final date', this.state.finaldate);
   this.props.navigation.push('Hotel', {
            startdate: this.state.startdate,

          });
 }

  render(){
    console.log('start date', this.state.startdate);

    return(
      <View style={styles.container}>
        <Card style={{margin:10}}>
  <ScrollView style={{margin:10}}>
                <Card>
                <Left>
                <Text style={{fontSize: 21,
                fontWeight: '600',
                marginBottom: 30,
                marginTop: 5,
                paddingLeft: 20,
                paddingRight: 20,
                margin: 5,
                padding:20,
              backgroundColor:'#ffffff'}}>
                Plan</Text>
                    </Left>
                </Card>
                <CardItem>
                <ImageBackground
                  style={{
                    alignSelf: 'center',
                    height: 150,
                    width: 400,
                  }}
                  source={require('../../src/img/klcc3.jpg')}
                  resizeMode="cover"
                >
                <View>

              </View>
              </ImageBackground>

                </CardItem>
                <Card style={{margin:10,padding:10}}>
                      <CardItem>
                        <Left>
                        <Body styele={{padding:80}}>
                                <Text style={styles.welcome}>Destination:  </Text>
                                <TouchableHighlight style={styles.des}>
                                <Text style={{
                                  fontSize: 25,
                                fontWeight: 'bold',
                              }}>Kuala Lumpur</Text>
                                </TouchableHighlight>

                        </Body>
                        </Left>
                      </CardItem>
                    </Card>

                      <CardItem>
                      <Text>Depart   :</Text>
                      <DatePicker
          style={{width: 200}}
          date={this.state.datetime1}
          mode="datetime"
          format="YYYY-MM-DD HH:mm"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0
            },
            dateInput: {
              marginLeft: 36
            }
          }}
          minuteInterval={10}
          onDateChange={(datetime) => {this.setState({startdate: datetime});}}
        />
                      </CardItem>





                  </ScrollView>
                  <Button
                  style={{marginLeft:150,padding:50}}
                  onPress={this._onNext()}>
                  <Text style={styles.btnText}>Next</Text>
                  </Button>
        </Card>

      </View>


    );
  }
}
export default PlannerDatePickerkl = createStackNavigator(
    {
      Pack : { screen: PlannerDatePickerkl},
      Hotel: {screen: PlacesHotelScreen},

    },
    {
      initialRouteName: 'Pack',
      navigationOptions: {
        header: null,
        gesturesEnabled: true,
      },// this is needed to make sure header is hidden
    }
  );


const styles = StyleSheet.create({
  bar:{
    flexDirection:'row',
  },
  sep:{
    borderRightWidth:2
  },
  container:{
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',

  },
  des:{
    alignItems:'center',
    margin:20,
    fontSize:20,
    color:'red'
  },
  button:{
    backgroundColor:'rgba(255, 255, 255, 0.3)',
    padding:18,
    flex:1,
    alignItems:'center'
  },
  btnText:{
    color: 'black',
     fontWeight: '500',
    fontSize: 20,
    alignItems: 'center',
    justifyContent: 'center',
   },


});
AppRegistry.registerComponent('datepicker', () => datepicker);
