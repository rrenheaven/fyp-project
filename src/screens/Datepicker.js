import React from 'react';
import { View, Text, StyleSheet,ScrollView,AsyncStorage, Image
  ,TouchableOpacity,Modal,TouchableHighlight,ImageBackground,AppRegistry,PanResponder,Alert} from "react-native";
import moment from 'moment';
import DateTimePicker from 'react-native-datepicker'
import Icon from '@expo/vector-icons/FontAwesome';
import { Card, CardItem,
   Body, Left, Right, Button } from 'native-base';
   import { createStackNavigator } from 'react-navigation';
   import PlaceArea from '../screens/PlaceArea';

class PlannerDatePickerkl extends React.Component{

  constructor(props) {
      super(props);

      this.state = {
        planid: '',
        planname:'',
        planimg:'',
        dataplan: [],
        enddate: '',
        startdate:'',

      };
 }

 _next = () => {

        const {startdate, enddate} = this.state
        const {navigation} = this.props
        const isDateEmpty = (startdate === "") || (enddate === "")

        if(isDateEmpty) return Alert.alert(
              'Alert',
              'Please Choose the Date and Time',
              [{text: '', onPress: () => console.log('OK Pressed')},],
              { cancelable: false }
          )

        const planid = navigation.getParam("planid", "");
        const planname = navigation.getParam("planname", "");

        return navigation.navigate('Area', {
            startdate,
            planid,
            planname,
            enddate
        })
    }


  render(){

    const { navigation } = this.props;
    const planname = navigation.getParam('planname', '');
    const planimg = navigation.getParam('planimg', '');

    return(

            <ScrollView style={{margin:10}}>
      //
                    <Card>
                        <Left>
                          <Text style={styles.title}>Plan</Text>
                        </Left>
                    </Card>

                            <Card>

                            <CardItem>
                            <TouchableOpacity onPress={this._showDateTimePicker}>
                          </TouchableOpacity>
                             <Text>Depart   :</Text>
                               <DateTimePicker
                                 style={{width: 200}}
                                   date={this.state.startdate}
                                   placeholder="select Date"
                                     mode="date"
                                      format="YYYY-MM-DD"
                                      minDate="2019-02-15"
                                      maxDate="2019-02-16"
                                        confirmBtnText="Confirm"
                                         cancelBtnText="Cancel"
                                         customStyles={{
                                           dateIcon: {
                                           position: 'absolute',
                                           left: 0,
                                           top: 4,
                                           marginLeft: 0
                                            },
                                        dateInput: {
                                      marginLeft: 36
                                    }
                                  }}
                                  onDateChange = {(value)=> this.props.startdateplan(value,index)}
                              onDateChange={(datetime) => {this.setState({startdate: datetime})}}
                             />
                            </CardItem>

                            <CardItem>
                            <TouchableOpacity onPress={this._showDateTimePicker}>
                          </TouchableOpacity>
                             <Text>End Date   :</Text>
                               <DateTimePicker
                                 style={{width: 200}}
                                   date={this.state.enddate}
                                   placeholder="select End Date"
                                     mode="date"
                                      format="YYYY-MM-DD"
                                      minDate="2019-02-15"
                                      maxDate="2019-02-16"
                                        confirmBtnText="Confirm"
                                         cancelBtnText="Cancel"
                                         customStyles={{
                                           dateIcon: {
                                           position: 'absolute',
                                           left: 0,
                                           top: 4,
                                           marginLeft: 0
                                            },
                                        dateInput: {
                                      marginLeft: 36
                                    }
                                  }}
                                  onDateChange = {(value)=> this.props.enddateplan(value,index)}
                              onDateChange={(datetime1) => {this.setState({enddate: datetime1})}}
                             />
                            </CardItem>

                            </Card>
      //
      <Card>

      <Button
        style={{marginLeft:130,padding:50}}
          onPress={() => this._next()}>
        <Text style={styles.btnText}>Next</Text>
      </Button>
      </Card>

                </ScrollView>

    );
  }
}

export default App = createStackNavigator(
{
Date : { screen: PlannerDatePickerkl},
Area: {screen: PlaceArea},
},{
initialRouteName: 'Date',
navigationOptions: {
header: null,
gesturesEnabled: true,
},// this is needed to make sure header is hidden
}
);



const styles = StyleSheet.create({
  bar:{
    flexDirection:'row',
  },
  sep:{
    borderRightWidth:2
  },
  title:
  {
    fontSize: 21,
    fontWeight: '600',
    marginBottom: 30,
    marginTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    margin: 5,
    padding:32,
  backgroundColor:'#ffffff'
},
  container:{
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',

  },
  des:{
    alignItems:'center',
    margin:20,
    fontSize:20,
    color:'red'
  },
  button:{
    backgroundColor:'rgba(255, 255, 255, 0.3)',
    padding:18,
    flex:1,
    alignItems:'center'
  },
  btnText:{
    color: 'black',
     fontWeight: '500',
    fontSize: 20,
    alignItems: 'center',
    justifyContent: 'center',
   },


});
