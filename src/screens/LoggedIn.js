import React, { Component } from 'react';
import LoggedInTabNavigator from '../navigators/LoggedInTabNavigator';
import transparentHeaderStyle from '../styles/navigation';

export default class LoggedIn extends Component{

static navigationOptions = () => ({
  headerLeft:null,
  headerStyle:transparentHeaderStyle,
  gesturesEnabled: false,
});



  render() {
    return(
      <LoggedInTabNavigator/>
    );
  }
}
