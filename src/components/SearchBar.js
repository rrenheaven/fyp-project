import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { Text, StyleSheet, View, ListView, TextInput, ActivityIndicator, Alert } from 'react-native';
import colors from '../styles/colors';
import PackagesCon from '../containers/PackagesContainer';
import { createStackNavigator } from 'react-navigation';

class SearchBar extends Component {
  constructor(props) {

    super(props);

    this.state = {

      isLoading: true,
      text: '',

    }

    this.arrayholder = [] ;
  }

  componentDidMount() {

    return fetch('http://192.168.1.253/User_Project/bigpack_list.php')
      .then((response) => response.json())
      .then((responseJson) => {
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.setState({
          isLoading: false,
          dataSource: ds.cloneWithRows(responseJson),
        }, function() {

          // In this block you can do something with new state.
          this.arrayholder = responseJson ;

        });
      })
      .catch((error) => {
        console.error(error);
      });

  }

  GetListViewItem (pack_name) {

    this.props.navigation.navigate("Inbox", {
      dataHo: this.state.dataHo, //store page data

    });
  }



   SearchFilterFunction(text){

     const newData = this.arrayholder.filter(function(item){
         const itemData = item.pack_name.toUpperCase()
         const textData = text.toUpperCase()
         return itemData.indexOf(textData) > -1
     })
     this.setState({
         dataSource: this.state.dataSource.cloneWithRows(newData),
         text: text
     })
 }

  ListViewItemSeparator = () => {
    return (
      <View
        style={{
          height: .5,
          width: "100%",
          backgroundColor: "#000",
        }}
      />
    );
  }


  render() {
    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, paddingTop: 20}}>
          <ActivityIndicator />
        </View>
      );
    }

    return (

      <View style={styles.MainContainer}>

      <TextInput
       style={styles.TextInputStyleClass}
       onChangeText={(text) => this.SearchFilterFunction(text)}
       value={this.state.text}
       underlineColorAndroid='transparent'
       placeholder="Search Here"
        />

        <ListView

          dataSource={this.state.dataSource}

          renderSeparator= {this.ListViewItemSeparator}

          renderRow={(rowData) =>

           <Text style={styles.rowViewContainer}
            onPress={this.GetListViewItem.bind(this, rowData.pack_name)} >
            {rowData.pack_name}
          </Text>}

          enableEmptySections={false}

          style={{marginTop: 10,height:10}}

        />

      </View>
    );
  }
}

export default SearchBar = createStackNavigator(
    {
      Pack : { screen: SearchBar},
      Inbox: {screen: PackagesCon},

    },
    {
      initialRouteName: 'Pack',
      navigationOptions: {
        header: null,
        gesturesEnabled: true,
      },// this is needed to make sure header is hidden
    }
  );

const styles = StyleSheet.create({

 MainContainer :{

  justifyContent: 'center',
  flex:1,
  height:10

  },

 rowViewContainer: {
   fontSize: 17,
   padding: 10,

  },

  TextInputStyleClass:{

   textAlign: 'center',
   height: 40,
   borderWidth: 1,
   borderColor: '#009688',
   borderRadius: 7 ,
   backgroundColor : "#FFFFFF"

   }

});
