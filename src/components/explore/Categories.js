import React, { Component } from 'react';
import {
  ScrollView,
  StyleSheet,
  TouchableHighlight,
  Image,
  View,
  Text,
  ImageBackground,
  TouchableOpacity
} from 'react-native';
import colors from '../../styles/colors';
import iPhoneSize from '../../helpers/utils';
import { createStackNavigator } from 'react-navigation';

const size = iPhoneSize();
let cardSize = 100;
let cardMargin = 8;

if (size === 'small') {
  cardSize = 90;
  cardMargin = 4;
} else if (size === 'large') {
  cardSize = 115;
}

export default class Categories extends Component {


  render() {
  	return (
    <ScrollView>

    <TouchableHighlight
    style={styles.card}
    onPress={() => LinkingIOS.openURL('https://website.com')}>
        <ImageBackground
        imageStyle={{ resizeMode: 'cover' }}
        source={require('../../../src/img/klcc3.jpg')}
        style={styles.image}
      >
          <View>
          <Text style={{
            fontSize: 20,
          fontWeight: 'bold',
          paddingLeft: 20,
          paddingTop: 20,
          color: colors.white}}>
            KUALA LUMPUR
          </Text>
        </View>
        </ImageBackground>
    </TouchableHighlight>

    <TouchableHighlight
    style={styles.card}
    onPress={() => LinkingIOS.openURL('https://website.com')}>
        <ImageBackground
        imageStyle={{ resizeMode: 'cover' }}
        source={require('../../../src/img/m.jpg')}
        style={styles.image}
      >
          <View>
          <Text style={{
            fontSize: 20,
          fontWeight: 'bold',
          paddingLeft: 20,
          paddingTop: 20,
          color: colors.white}}>
            Melaka
          </Text>
        </View>
        </ImageBackground>
    </TouchableHighlight>

    <TouchableHighlight
    style={styles.card}
    onPress={() => LinkingIOS.openURL('https://website.com')}>
        <ImageBackground
        imageStyle={{ resizeMode: 'cover' }}
        source={require('../../../src/img/p.jpg')}
        style={styles.image}
      >
          <View>
          <Text style={{
            fontSize: 20,
          fontWeight: 'bold',
          paddingLeft: 20,
          paddingTop: 20,
          color: colors.white}}>
            Penang
          </Text>
        </View>
        </ImageBackground>
    </TouchableHighlight>



    </ScrollView>
  	);
  }
}

const styles = StyleSheet.create({
  wrapper: {
  	flex: 1, flexDirection: 'row', flexWrap: 'wrap'
  },
  card: {
    display: 'flex',
    flexDirection: 'row',
    width: 500,
    justifyContent:'space-between',
    padding:10,
    height: 200,


  },
  image: {
    flex: 1,
    width: undefined,
    height: 180,
    padding:10,
    justifyContent:'space-between',

  },
});
