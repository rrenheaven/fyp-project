import React, { Component } from 'react';
import AccordioPlanner from '../plan/AccordioPlanner';
import {StyleSheet,Text,View,AppRegistry,TouchableHighlight,
  ListView,Animated,ScrollView,TouchableOpacity,ActivityIndicator,Image, FlatList, Alert, YellowBox}
from 'react-native';

class Planlist extends Component {
  constructor(props) {

     super(props);

     this.state = {

       isLoading: true

     }

     YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

   }


   FlatListItemSeparator = () => {
     return (
       <View
         style={{
           height: 20,
           width: "100%",
           backgroundColor: "#000",
         }}
       />
     );
   }

   webCall=()=>{

    return fetch('http://192.168.43.132/User_Project/planner_list.php')
           .then((response) => response.json())
           .then((responseJson) => {
             this.setState({
               isLoading: false,
               dataSource: responseJson
             }, function() {
               // In this block you can do something with new state.
             });
           })
           .catch((error) => {
             console.error(error);
           });

   }

   componentDidMount(){

    this.webCall();

   }


  render() {
    return(
      <View style={{backgroundColor:'#f2f2f2'}}>
        <ScrollView>

        <Text>Suggested</Text>
        <FlatList
             data={ this.state.dataSource }
             ItemSeparatorComponent = {this.FlatListItemSeparator}
             renderItem={({item}) =>
          <AccordioPlanner title="Is this Header text">
            <Text style={styles.heading2}>{item.Hotel_detail}</Text>
          <Text>CHECK-IN : 12:00am</Text>
            <Text style={styles.heading2}>{item.Rest_detail}</Text>
          <Text>OPEN : 9:00am</Text>
            <Text style={styles.heading2}>{item.Site_detail}</Text>

          </AccordioPlanner>

        }
                    keyExtractor={(item, index) => index.toString()}

              />


        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  heading1: {
    fontSize: 15,
    fontWeight: '550',
padding:20
  },
   heading2: {
      fontSize: 12,
      fontWeight: '550',
      color: 'black'
    },
    });
    export default Planlist;
AppRegistry.registerComponent('Planlist', () => Planlist);
