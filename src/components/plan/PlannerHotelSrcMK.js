import React from 'react';
import { View, Text, StyleSheet,AsyncStorage,FlatList,ScrollView,Image,Modal,TouchableHighlight,TouchableOpacity,YellowBox} from "react-native";
import { Card, CardItem, Thumbnail,
   Body, Left, Right, Button } from 'native-base';
import Icon from '@expo/vector-icons/FontAwesome';
import { createStackNavigator } from 'react-navigation';
import PlannerResSrcMK from '../plan/PlannerResSrcMK';

class PlannerHotelSrcMK extends React.Component{

  constructor(props) {

     super(props);

     this.state = {
       isLoading: true,
       startdate:'',

     }

     YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

   }

  _Addhotel(){
    // console.log('Add',this.state.dataHo);

    this.setState({
      dataHo: this.state.hoteldata
    })
  }

  _hoteldata() {
      console.log("start date", this.props.navigation.state.dataHo);
      const startdate = this.props.navigation.getParam("startdate", "datetime");

      this.props.navigation.navigate("Inbox", {
        dataHo: this.state.dataHo, //store page data
        startdate
      });
    }

  componentDidMount(){

   return fetch('http://192.168.1.253/User_Project/place.php')
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({
              isLoading: false,
              hoteldata: responseJson
            }, function() {
              // In this block you can do something with new state.
            });
          })
          .catch((error) => {
            console.error(error);
          });
          this.setState({
            dataH:
             ((typeof this.props.navigation.state.params == 'undefined')) ? '' : this.props.navigation.state.params.dataH,
          })


  }





  render(){
    console.log('Hotel',this.props.navigation);
    const { navigation } = this.props;
    const { params } = this.props.navigation;
    const startdate = navigation.getParam('startdate', 'datetime');
    const dataHo = navigation.getParam('dataHo', 'some default value');

    return(
      <ScrollView>

               <Card>
               <Text style={styles.title}>Pick A Hotel</Text>
               <ScrollView style={styles.scrollView}>
       <FlatList
         data={ this.state.hoteldata }
         renderItem={({item}) =>

               <Card style={{margin:10,padding:10}}>
                   <CardItem>
               //each compoennt need wrapper by one carditem
                       <Left>
                           <Body>

                           <Text>
                           <Text style={styles.heading1}>{item.hotel_name}</Text>
                           </Text>
                           <Text note>
        Novemebr 23, 2018
                           </Text>
                           </Body>
                       </Left>
                    </CardItem>
                   <Card>
                   <View>
                     <Image source = {{ uri: item.hotel_img }} style={styles.imageView} />
                </View>
              </Card>

              <CardItem>
                                         <Text style={styles.description}>
                                         <Text style={styles.heading2}>{item.hotel_detail}</Text>
                                         </Text>
                             </CardItem>
                             <CardItem>
                             <Text style={styles.description}>
                             <Text style={styles.heading2}>{item.hotel_detail2}</Text>
                             </Text>
                             </CardItem>

                             <CardItem style={{height:50, paddingLeft:250}}>
                             <Left>
                                 <Button


                                   onPress={() => this._Addhotel()}>
                                  <Text style={{padding:50}}
                                    >
                                       Add

                                  </Text>
                               </Button>

                             </Left>
                             </CardItem>


               </Card>

                           }
                   keyExtractor={(item, index) => index.toString()}

         />



         <CardItem style={{height:50, paddingLeft:250}}>
         <Left>
             <Button


               onPress={() => this._hoteldata()}>
              <Text style={{padding:50}}
                >
                   Next

              </Text>
           </Button>

         </Left>
         </CardItem>




                         </ScrollView>

               </Card>
               </ScrollView>





             );
           }
         }



export default PlannerHotelSrcMK = createStackNavigator(
    {
      Pack : { screen: PlannerHotelSrcMK},
      Inbox: {screen: PlannerResSrcMK},

    },
    {
      initialRouteName: 'Pack',
      navigationOptions: {
        header: null,
        gesturesEnabled: true,
      },// this is needed to make sure header is hidden
    }
  );

const styles = StyleSheet.create({
  container:{
    flex: 1,
    alignItems:'center',
    justifyContent:'center'
  },
  title:
  {
    fontSize: 21,
    fontWeight: '600',
    marginBottom: 30,
    marginTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    margin: 5,
    padding:32,
  backgroundColor:'#ffffff'
},
bar:{
      flexDirection:'row',
    },  sep:{
        borderRightWidth:2
      },button:{
        backgroundColor:'rgba(255, 255, 255, 0.3)',
        padding:10,
        flex:1,
        alignItems:'center'
      },
      btnText:{
    		color:'black',fontWeight:'bold'
    	},
      imageView: {
          width: '100%',
          height: 250 ,
          margin: 7,
          borderRadius : 7
       },

});
