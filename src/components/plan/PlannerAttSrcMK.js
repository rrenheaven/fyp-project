import React from 'react';
import { View, Text, StyleSheet,AsyncStorage,ScrollView,Image,Modal,TouchableHighlight,TouchableOpacity,FlatList,PanResponder,YellowBox} from "react-native";
import { Card, CardItem, Thumbnail,
   Body, Left, Right, Button } from 'native-base';
import Icon from '@expo/vector-icons/FontAwesome';
import { createStackNavigator } from 'react-navigation';

import Planner from '../../containers/Planner_state';

class PlannerAttSrcMK extends React.Component{
  constructor(props) {

     super(props);

     this.state = {
       isLoading: true

     }

     YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

   }

  _AddAtt(){
    console.log('Add',this.state.attdata);

    this.setState({
      dataAtt: this.state.attdata[0].att_name
    })
  }

  _Attdata(){
    console.log('start date', this.props.navigation.state.dataRe, );
    const startdate = this.props.navigation.getParam("startdate", "datetime");
    const dataHo = this.props.navigation.getParam("dataHo", "hotel");
    const dataRe = this.props.navigation.getParam("dataRe", "restaurant");

    this.props.navigation.navigate('Inbox', {
          dataAtt: this.state.dataAtt,//store page data
          dataRe,
          dataHo,
           startdate
           })


  }

  componentDidMount(){

   return fetch('http://192.168.1.253/User_Project/place_att.php')
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({
              isLoading: false,
            attdata: responseJson
            }, function() {
              // In this block you can do something with new state.
            });
          })
          .catch((error) => {
            console.error(error);
          });
          this.setState({
            dataH:
             ((typeof this.props.navigation.state.params == 'undefined')) ? '' : this.props.navigation.state.params.dataH,
          })


  }




  render(){
    // console.log('Add',this.state.hoteldata);
    const { navigation } = this.props;
    const startdate = navigation.getParam('startdate', '');
    const dataHo = navigation.getParam('dataHo', '');
    const dataRe = navigation.getParam('dataRe', '');

    return(
      <ScrollView>

               <Card>
               <Text style={{fontSize: 21,
               fontWeight: '600',
               marginBottom: 30,
               marginTop: 5,
               paddingLeft: 20,
               paddingRight: 20,
               margin: 5,
               padding:32,
             backgroundColor:'#ffffff'}}>
               Pick A Attraction



               </Text>
               <ScrollView style={styles.scrollView}>
       <FlatList
         data={ this.state.attdata }


         renderItem={({item}) =>

               <Card style={{margin:10,padding:10}}>
                   <CardItem>
               //each compoennt need wrapper by one carditem
                     <Left>
                         <Body>

                         <Text>
                         <Text style={styles.heading1}>{item.att_name}</Text>
                         </Text>
                         <Text note>
      Novemebr 23, 2018
                         </Text>
                         </Body>
                     </Left>
                   </CardItem>
                   <Card>
                   <View>
                     <Image source = {{ uri: item.att_img }} style={styles.imageView} />
                   </View>
                    </Card>

                             <CardItem>
                                         <Text style={styles.description}>
                                         <Text style={styles.heading2}>{item.att_detail}</Text>
                                         </Text>
                             </CardItem>


                             <CardItem style={{height:50, paddingLeft:250}}>
                             <Left>
                                 <Button


                                   onPress={() => this._AddAtt()}>
                                  <Text style={{padding:50}}
                                    >
                                       Add

                                  </Text>
                               </Button>

                             </Left>
                             </CardItem>


               </Card>

                           }
                   keyExtractor={(item, index) => index.toString()}

         />



         <CardItem style={{height:50, paddingLeft:250}}>
         <Left>
             <Button


               onPress={() => this._Attdata()}>
              <Text style={{padding:50}}
                >
                   Confirm

              </Text>
           </Button>

         </Left>
         </CardItem>

                         </ScrollView>

               </Card>
               </ScrollView>





             );
           }
         }



  export default PlannerAttSrcMK = createStackNavigator(
    {
      Pack : { screen: PlannerAttSrcMK},
      Inbox: {screen: Planner},

    },
    {
      initialRouteName: 'Pack',
      navigationOptions: {
        header: null,
        gesturesEnabled: true,
      },// this is needed to make sure header is hidden
    }
  );

  const styles = StyleSheet.create({
    title:
    {
      fontSize: 21,
      fontWeight: '600',
      marginBottom: 30,
      marginTop: 5,
      paddingLeft: 20,
      paddingRight: 20,
      margin: 5,
      padding:32,
    backgroundColor:'#ffffff'
  },
  container:{
    flex: 1,
    alignItems:'center',
    justifyContent:'center'
  },  bar:{
      flexDirection:'row',
    },  sep:{
        borderRightWidth:2
      },button:{
        backgroundColor:'rgba(255, 255, 255, 0.3)',
        padding:10,
        flex:1,
        alignItems:'center'
      },
      btnText:{
        color:'black',fontWeight:'bold'
      },
      imageView: {
          width: '100%',
          height: 250 ,
          margin: 7,
          borderRadius : 7
       },

  });
