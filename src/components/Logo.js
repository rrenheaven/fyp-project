import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

export default class Logo extends React.Component{
  render(){
    return(
        <View style={styles.container}>

         <Image style={{width:180,height:150}}
         source={require('../img/airbnb-logo.png') }
       />
          <Text style={styles.logoText}>Welcome </Text>
          </View>
    )
  }
}

const styles =StyleSheet.create({
  container :{
    flex:1,
    justifyContent:'center',
    alignItems:'center',


  },
  logoText:{

    fontSize:18,
    color:'rgba(255, 255, 255, 0.7)',
    margin:100
  }
})
