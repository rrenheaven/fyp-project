import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  ScrollView,
TouchableOpacity,FlatList,Alert,YellowBox

} from 'react-native';
import colors from '../../styles/colors';
import { Card, CardItem, Thumbnail,
   Body, Left, Right, Button } from 'native-base';
   import Image from 'react-native-scalable-image';
   import RoundedButton from '../../components/buttons/RoundedButton';
   import InboxContainer from '../../containers/InboxContainer';
   import { connect } from 'react-redux';
   import { bindActionCreators } from 'redux';
   import { PropTypes } from 'prop-types';
   import ActionCreators from '../../redux/actions';
   import Swiper from 'react-native-swiper';
   import LogIn from '../../screens/LogIn';
   import LoggedOut from '../../screens/LoggedOut';
   import { createStackNavigator } from 'react-navigation';
   import Packlist from '../../components/packages/packages_list'
   import Packages_state from '../../containers/Packages_state';



class PackagesKlcc extends Component {
  constructor(props){
      super(props)
      this.state = {
          packid: '',
          dataklcc: []//set data name as object at initial value
      }
  }


  async componentDidMount() {
      const packid = await this.props.navigation.getParam('packid');
      this.fetchObj({packid})

  }





// server part, can be do it later
fetchObj = async ({packid}) => {
    const api = 'http://192.168.1.253/User_Project/packages_List_klcc.php?packid=' + packid
    const method = 'POST'
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
    }
    const response = await fetch(api, {method, headers});//Fetch the resource
    // console.log(response)
    console.log(data)
    const text = await response.text();//Parse it as text
    const data = JSON.parse(text)//Try to parse it as json

    return this.setState({
        isLoading: false,
        dataklcc: data,


    })
    // console.log('Get', this.state.dataklcc)

}

  _addPlanner = async (obj) => {
    console.log(obj)
     // console.log('GetPack', this.props.navigation.state.dataPack);
     const dataPack = this.props.navigation.getParam("dataPack", "");
     this.props.navigation.navigate('Inbox', {
              dataklccHo: this.state.dataklcc,
              dataPack  //store page data
            })

   }

  render() {

    const {dataklcc} = this.state
    if(dataklcc.length === 0) return <Text>Downloading Data</Text>
    console.log('Get', this.state.dataklcc)

   // const { navigation } = this.props;
   //  const dataPack = navigation.getParam('dataPack', '');
   //  const packid = navigation.getParam('packid', '');
    // console.log('Get', packid);
  	return (
      <ScrollView style={styles.scrollView}>
          <Card>
              {
                  // .map is to extract every single obj out from array
                  dataklcc.map((obj, index) => {
                      return (
                          <View key={index}>
                              <Text style={styles.heading1}>KLCC</Text>
                              <Hotel
                                <Text style={styles.heading1}>{obj.smallpackcon_name}</Text>
                              />
                              <Resturants
                                  <Text style={styles.heading1}>{obj.smallpackcon_name}</Text>
                              />
                              <Shop
                                  <Text style={styles.heading1}>{obj.smallpackcon_name}</Text>
                              />
                              <DetailButton
                                  ButtonLabel = 'More...'
                                  onPress = {() => _addPlanner(obj)} //pass this package(object) to planner page
                              />
                          </View>
                      )
                  })
              }
          </Card>
      </ScrollView>

  )
}
}


const Hotel = ({name, price}) => {

// each component should have it own style sheet
const style = StyleSheet.create({
  heading: {
      fontSize: 18,

  },
  price: {
      color: '#222222'
  }
})

return (
  <CardItem>
      <Left>
          <Body>
              <Text>
                  <Text style={style.heading}>{name}</Text>
              </Text>
              <Text style={style.price} note>{price}</Text>
          </Body>
      </Left>
  </CardItem>
)
}


const Resturants = ({name, address}) => {
return (
  <Card>
      <Text>{name}</Text>
      <Text>{address}</Text>
  </Card>
)
}


const Shop = ({name, rating}) => {
return (
  <CardItem>
      <Text>
          <Text >{name}</Text>
      </Text>
      <Text>{rating}</Text>
  </CardItem>
)
}

const DetailButton = ({ButtonLabel, onPress}) => {
return (
  <CardItem style={{height:50, paddingLeft:280}}>
      <Left>
          <Button onPress={() => onPress()}>
              <Text>{ButtonLabel}</Text>
          </Button>
      </Left>
  </CardItem>
)
}


export default App = createStackNavigator(
{
Pack : { screen: PackagesKlcc},
Inbox: {screen: Packages_state},
},{
initialRouteName: 'Pack',
navigationOptions: {
header: null,
gesturesEnabled: true,
},// this is needed to make sure header is hidden
}
);

const styles = StyleSheet.create({
  wrapper: {
     height:80,
      width:220 ,
      borderRadius : 7

},
text:{
  fontSize: 21,
  fontWeight: '600',
  marginBottom: 30,
  marginTop: 5,
  paddingLeft: 20,
  paddingRight: 20,
  margin: 5,
  padding:32,
backgroundColor:'#ffffff'
},
  MainContainer: {
    justifyContent: 'center',
   flex:1,
   margin: 10,
 },
 context:{
   textAlignVertical:'center',
flex:8,
margin:10
},
imageView: {
    width: '100%',
    height: 20 ,
    margin: 7,
    borderRadius : 7
 },
  heading: {
    fontSize: 20,
    fontWeight: '500',
    marginBottom: 30,
    marginTop: 40,
    paddingLeft: 20,
    paddingRight: 20,
  },
  heading1: {
    fontSize: 15,
    fontWeight: '550',
    marginBottom: 30,
    marginTop: 40,
    paddingLeft: 20,
    paddingRight: 20,
  },
   heading2: {
      fontSize: 12,
      fontWeight: '550',
      color: '#000'
    },
  description: {
    fontSize: 12,
    lineHeight: 18,
    color: colors.gray04,
    paddingLeft: 10,
    paddingRight: 10,
  },
  footer: {
   position: 'absolute',
   width: '100%',
   height: 80,
   bottom: 0,
   borderTopWidth: 1,
   borderTopColor: colors.gray05,
   paddingLeft: 20,
   paddingRight: 20,
  },
  findHomesButton: {
   paddingTop: 15,
   paddingBottom: 15,
   marginTop: 16,
   borderRadius: 3,
   backgroundColor: colors.pink,
  },
  findHomesButtonText: {
    color: colors.white,
    textAlign: 'center',
    fontWeight: '600',
  },
});
