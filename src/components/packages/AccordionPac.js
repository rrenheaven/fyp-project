import React, { Component } from 'react';
import {StyleSheet,Text,View,TouchableHighlight,Animated}
from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default  class Accordio extends Component{
  constructor(props){
      super(props);
      this.icons = {
          'open'    : 'sort-down',
          'close'  : 'sort-up'
      };

      this.state = { expanded : false };
  }

  toggle(){
      this.setState({
          expanded : !this.state.expanded
      });
  }
  render(){
      let icon = this.icons['open'];
      if(this.state.expanded){
          icon = this.icons['close'];
      }
      return (
          <View
              style={styles.container}>
              <View style={styles.titleContainer}>
              <Text style={styles.Header}>Packages</Text>
                  <TouchableHighlight
                      style={styles.button}
                      onPress={this.toggle.bind(this)}
                      underlayColor="#f1f1f1">
                      <Icon
                          style={styles.FAIcon}
                          name={icon}
                      />
                  </TouchableHighlight>


              </View>

             {
                 this.state.expanded && ( <View style={styles.body}>
                                              {this.props.children}
                                          </View>)
             }

          </View>
      );
  }
}

var styles = StyleSheet.create({
  container   : {
      backgroundColor: "white",
      margin:20,
      overflow:'hidden',
      borderRadius:10,

  },
  titleContainer : {
      flexDirection: 'row',
      justifyContent:'center',
      alignItems:'center'
  },
  Header : {
      flex    : 1,
      padding : 10,
       color: 'black'
  },
  FAIcon : {
      height  : 40,
      color:"black",
      marginRight:5

  },
  body        : {
      padding     : 10,
      paddingTop  : 0,

  }
});
