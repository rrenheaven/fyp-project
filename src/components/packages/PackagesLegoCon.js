import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  ScrollView,
TouchableOpacity,ActivityIndicator, FlatList, Alert, YellowBox
} from 'react-native';
import colors from '../../styles/colors';
import { Card, CardItem, Thumbnail,
   Body, Left, Right, Button } from 'native-base';
   import Image from 'react-native-scalable-image';
   import RoundedButton from '../../components/buttons/RoundedButton';
   import { connect } from 'react-redux';
   import { bindActionCreators } from 'redux';
   import { PropTypes } from 'prop-types';
   import ActionCreators from '../../redux/actions';
   import Swiper from 'react-native-swiper';
   import LogIn from '../../screens/LogIn';
   import LoggedOut from '../../screens/LoggedOut';

export default class NoResults extends Component {
  constructor(props) {

     super(props);

     this.state = {

       isLoading: true

     }

     YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

   }


   FlatListItemSeparator = () => {
     return (
       <View
         style={{
           height: 20,
           width: "100%",
           backgroundColor: "#000",
         }}
       />
     );
   }

   webCall=()=>{

    return fetch('http://192.168.1.253/User_Project/packages_List.php')
           .then((response) => response.json())
           .then((responseJson) => {
             this.setState({
               isLoading: false,
               dataSource: responseJson
             }, function() {
               // In this block you can do something with new state.
             });
           })
           .catch((error) => {
             console.error(error);
           });

   }

   componentDidMount(){

    this.webCall();

   }


  render() {

  	return (
      <View style={styles.MainContainer}>

      <Text style={styles.heading}>Legoland ,Malaysia</Text>
      <FlatList

       data={ this.state.dataSource }

       ItemSeparatorComponent = {this.FlatListItemSeparator}

       renderItem={({item}) =>
          <View style={{flex:1, flexDirection: 'row'}}>
          <View>

                <Swiper style={styles.wrapper} showsButtons={true} loop={false}>
                  <View>
                  <Image source = {{ uri: item.packages_image }} style={styles.imageView} />
                  </View>
                  <View>
                  <Image source = {{ uri: item.packages_image1 }} style={styles.imageView} />
                  </View>
                  <View>
                  <Image source = {{ uri: item.packages_image2 }} style={styles.imageView} />
                  </View>

                </Swiper>
                </View>

                <View style={styles.context}>
                <Text style={styles.heading1}>{item.packages_name}</Text>
                <Text style={styles.heading2}>{item.packages_detail}</Text>
               </View>


          </View>
                                  }
              keyExtractor={(item, index) => index.toString()}

        />
        <View  style={{height:50, paddingLeft:300}}>

                      <Button

                          title="Go to Details"
                          onPress={() => this.props.navigation.navigate('Details')}>
                         <Text>
                         ADD To Planner
                        </Text>
                      </Button>

            </View>
                               </View>
                             );
                           }
                          }




  //



const styles = StyleSheet.create({
  wrapper: {
     height:80,
      width:220 ,
      borderRadius : 7

},
  MainContainer: {
    justifyContent: 'center',
   flex:1,
   margin: 10,
 },
 context:{
   textAlignVertical:'center',
flex:8,
margin:10
},
imageView: {
    width: '100%',
    height: 20 ,
    margin: 7,
    borderRadius : 7
 },
  heading: {
    fontSize: 20,
    fontWeight: '500',
    marginBottom: 30,
    marginTop: 40,
    paddingLeft: 20,
    paddingRight: 20,
  },
  heading1: {
    fontSize: 15,
    fontWeight: '550',
    marginBottom: 30,
    marginTop: 40,
    paddingLeft: 20,
    paddingRight: 20,
  },
   heading2: {
      fontSize: 12,
      fontWeight: '550',
      color: '#000'
    },
  description: {
    fontSize: 12,
    lineHeight: 18,
    color: colors.gray04,
    paddingLeft: 10,
    paddingRight: 10,
  },
  footer: {
   position: 'absolute',
   width: '100%',
   height: 80,
   bottom: 0,
   borderTopWidth: 1,
   borderTopColor: colors.gray05,
   paddingLeft: 20,
   paddingRight: 20,
  },
  findHomesButton: {
   paddingTop: 15,
   paddingBottom: 15,
   marginTop: 16,
   borderRadius: 3,
   backgroundColor: colors.pink,
  },
  findHomesButtonText: {
    color: colors.white,
    textAlign: 'center',
    fontWeight: '600',
  },
});
