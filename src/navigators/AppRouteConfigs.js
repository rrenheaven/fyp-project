import {
    createStackNavigator,
  } from 'react-navigation';
  import LoggedOut from '../screens/LoggedOut';
  import LogIn from '../screens/LogIn';
  import ForgotPassword from '../screens/ForgotPassword';
  import LoggedInTabNavigator from './LoggedInTabNavigator';
  import SignUpScreen from '../screens/signup';
  import LoggedIn from '../screens/LoggedIn';
  import PackagesContainer from '../containers/PackagesContainer';
  import PackagesLegoCon from '../components/packages/PackagesLegoCon';



  const AppRouteConfigs = createStackNavigator({
    LoggedOut: { screen: LoggedOut },
    LoggedIn: {
               screen: LoggedIn,
               navigationOptions: {
                 header: null,
                 gesturesEnabled: false,
               },// this is needed to make sure header is hidden
              },
   LogIn: { screen: LogIn },
   ForgotPassword: { screen: ForgotPassword },
   Signup: { screen: SignUpScreen },
   PackagesContainer:{screen: PackagesContainer},
  PackagesLegoCon:{ screen:PackagesLegoCon}
   // TurnOnNotifications: { screen: TurnOnNotifications },
 });

 export default AppRouteConfigs;
