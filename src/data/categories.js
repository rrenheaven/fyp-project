const categoriesList = [
  {
  	title: 'KUALA LUMPUR',
    photo:require('../img/klcc3.jpg'),
  },
  {
  	title: 'MELAKA',
  	photo:require('../img/m.jpg'),
  },
  {
  	title: 'PENANG',
    photo:require('../img/p.jpg'),
  },
];

export default categoriesList;
