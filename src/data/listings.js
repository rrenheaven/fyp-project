import colors from '../styles/colors';

const listing1Photo = require('../img/klcc3.jpg');
const listing2Photo = require('../img/p.jpg');
// const listing3Photo = require('./photos/listing3.png');
// const listing4Photo = require('./photos/listing4.png');
// const listing5Photo = require('./photos/listing5.png');
//const listing6Photo = require('./photos/listing6.png');
//const listing8Photo = require('./photos/listing8.png');
//const listing9Photo = require('./photos/listing9.png');
//const listing10Photo = require('./photos/listing10.png');
//const listing11Photo = require('./photos/listing11.png');
//const listing12Photo = require('./photos/listing12.png');
//const listing13Photo = require('./photos/listing13.png');
//const listing14Photo = require('./photos/listing14.png');
//const listing15Photo = require('./photos/listing15.png');
const listing16Photo = require('../img/m.jpg');

const listings = [
  {
    title: '',
    boldTitle: true,
    showAddToFav: true,
    listings: [
      {
        id: 1,
        photo: listing1Photo,
        // type: 'BOAT RIDE',
        title: 'Hotel Legoland Malaysia',
        location: 'Johor, Malaysia ',
        price: 60,
        priceType: 'per room',
        stars: 29,
        color: colors.gray04,
      },

      {
        id: 2,
       photo: listing2Photo,
        // type: 'RESERVATION',
        title: 'Bar Boulud',
        price: '46',
        priceType: 'per person',
        stars: 0,
        color: colors.gray04,
      },
      {
        id: 3,
       photo: listing16Photo,
        // type: 'RESERVATION',
        title: 'Bar Boulud',
        price: '46',
        priceType: 'per room',
        stars: 2,
        color: colors.gray04,
      },
    ],
  },
];

export default listings;
